FROM ros:noetic

# change this based on your location
ENV TZ=America/Los_Angeles

ENV DEBIAN_FRONTEND noninteractive # build hangs on timezone & other prompts otherwise

RUN apt-get update && apt-get install -y --no-install-recommends \
  lsb-release \
  git-all\
  gnupg2 \
  keyboard-configuration \
  software-properties-common \
  vim \
  wget \
  nano 

RUN apt-get update && apt-get -y install python3 python3-pip python3-opencv ffmpeg graphviz ros-noetic-tf


RUN apt-get update && apt-get install -y --no-install-recommends \
  python3-pip \
  gcc \ 
  g++ \ 
  gfortran \
  cmake \
  liblapack-dev \
  pkg-config \
  net-tools \
  libboost-all-dev \
  libserial-dev \
  libdlib-dev \
  libeigen3-dev \
  coinor-libipopt-dev \
  libasound2 \
  autotools-dev \
  automake \
  flex

RUN apt-get clean all

RUN /bin/bash -c "cd /root ;git clone https://github.com/FrancescoSaverioZuppichini/RosbagPandas.git"
RUN /bin/bash -c "cd /root/RosbagPandas/ ;python3 setup.py install"


# ROS workspace setup (for needed packages, e.g., webots_ros)
RUN mkdir -p /catkin_ws/src \
&& cd /catkin_ws/src/ \
&& git clone https://github.com/cyberbotics/webots_ros.git \
&& cd /catkin_ws/src/webots_ros \
&& git checkout 673fba8a7f

RUN cd /catkin_ws/ \
&& . /opt/ros/noetic/setup.sh \
&& catkin_make \
&& . /catkin_ws/devel/setup.sh



RUN pip3 install --upgrade pip

# install all packages needed by the pipeline
#ADD requirements2.txt /root/requirements2.txt
#RUN pip3 install -r /root/requirements2.txt

RUN apt-get update && apt-get install -y nodejs npm python3-tf

#RUN python3 -m pip install numpy pandas opencv-python tqdm jupyter jupyterlab scikit-image scikit-learn seaborn natsort h5py scipy chart_studio catkin_pkg k3d #tensorflow-gpu Keras torchvision poutyne comet comet_ml imgaug torchsummary logger psutil

RUN pip3 install numpy pandas opencv-python tqdm jupyter jupyterlab scikit-image scikit-learn seaborn natsort h5py scipy chart_studio catkin_pkg k3d #tensorflow-gpu Keras torchvision poutyne comet comet_ml imgaug torchsummary logger psutil mlflow laspy colorama

#RUN jupyter nbextension enable --py --sys-prefix k3d
#RUN jupyter labextension install jupyterlab-dash
#RUN jupyter labextension install --minimize=False @jupyter-widgets/jupyterlab-manager k3d

RUN pip3 install --upgrade poutyne

############# from here on only needed to configure donctainer to run
# cuda and opengl applications (computing or graphics), e.g., rviz from inside

## for opengl (with nvidia) support (for xapps)
## see https://medium.com/@benjamin.botto/opengl-and-cuda-applications-in-docker-af0eece000f1

RUN apt-get update && apt-get install -y --no-install-recommends \
        pkg-config \
        libxau-dev \
        libxdmcp-dev \
        libxcb1-dev \
        libxext-dev \
        libx11-dev \
        libglvnd0 \
        libgl1 \
        libglx0 \
        libegl1 \
        libxext6 \
        libx11-6 && \
        rm -rf /var/lib/apt/lists/*

RUN apt-get update && apt-get install -y build-essential autoconf libtool pkg-config freeglut3-dev glmark2 libxtst6

ENV NVIDIA_VISIBLE_DEVICES all
ENV NVIDIA_DRIVER_CAPABILITIES graphics,utility,compute

## end opengl support



CMD ["bash"]
