#!/usr/bin/env python
#
# implementation of service calls using the supervisor API of webots2ros
#
import sys
import rospy
import time
import numpy as np
import math
from webots_ros.srv import *
"""
Little abstraction for easy ros - webots2ros manipulation
Usage:

# Create a supervisor

>>> supervisor = Supervisor('ros_supervisor')

# create a Node

>>> node = supervisor.get_node_from_def.from_def('EL_GRID')

# get a field from that node

>>> heigh_field = node['height']

# get/set an element in that field

>>> el = heigh_field[0]
>>> heigh_field[3] = 3.326
"""


class Supervisor:
    """
    Interface to access all the services exposed by the Webots Supervisor class.
    This class can be inherited to automatically access all the methods,
    or it can be instantiated to be used and shared/
    """

    def __init__(self, name, verbose=True):
        """ Constructor.
        
        Args:
            name (str): name of the Webots robot. """

        if len(name) > 0 and name[0] != '/':
            self.name = '/' + name
        else:
            self.name = name

        self.verbose = verbose

    @staticmethod
    def get_service(service, type):
        rospy.wait_for_service(service, timeout=None)
        res = rospy.ServiceProxy(service, type)
        return res

    @staticmethod
    def call_service(url, type, *args, **kwargs):
        res = None
        try:
            service = Supervisor.get_service(url, type)
            res = service(*args, **kwargs)
        except Exception:
            if Supervisor.verbose: rospy.logwarn(url + ' ' +  str(e))

        finally:
            return res

    def retry_service(self, func, *args, **kwargs):
        res = None
        while res == None:
            res = func(*args, **kwargs)
            time.sleep(0.01)

        return res

    def get_node_from_def(self, node_def):
        """ Obtain a node from its Webots DEF name.

        Args:
            node_def DEF of the node
        
        Returns:
            A Node object. 
        """

        service = self.name + '/supervisor/get_from_def' 
        
        #print (">>>")
        #print (service, node_def)
        #print ("<<<")
        response = self.call_service(service, supervisor_get_from_def, node_def, 0)
        node_id = response.node

        if node_id == 0:
            raise KeyError('No node with def: {}'.format(node_def))
        else:
            return Node(node_id, self)

    def get_simulation_time(self):
        """ Returns the simulation time in seconds. """

        service = self.name + '/robot/get_time'
        res = self.call_service(service, get_float)
        return res

    def get_world_node(self):
        """ Return the root node of the world. """
        service = self.name + '/supervisor/get_root'
        # world = self.call_service(service, get_uint64)
        self.world = self.call_service(service, get_uint64)
        return self.world

    def load_world(self, world_name):
        service = self.name + '/supervisor/world_load'
        #print (service)
        res = self.call_service(service, set_string, world_name)
        return res

    def wait_for_simulation_time(self, duration):
        """ Block until the simulation has run for a given duration.

        Args:
            duration (float): Duration in seconds in simulation time
        """
        t0 = self.retry_service(self.get_simulation_time).value
        #print ("starting wait_for_simulation time: ", t0)
        while (self.retry_service(self.get_simulation_time).value - t0) < duration:
            time.sleep(0.05)
        #print ("ending wait_for_simulation time: ", self.retry_service(self.get_simulation_time).value)

    def reset_simulation(self):
        service = self.name + '/supervisor/simulation_reset'

        res = self.call_service(service, get_bool)

        return res

    def set_simulation_mode(self, mode):
        service = self.name + '/supervisor/simulation_set_mode'

        res = self.call_service(service, set_int, mode)

        return res

    def reset_simulation_physics(self):
        service = self.name + '/supervisor/simulation_reset_physics'

        res = self.call_service(service, get_bool)

        return res

    def reset_node_physics(self, node_id):
        service = self.name + '/supervisor/node/reset_physics'

        res = self.call_service(service, node_reset_functions, node_id)

        return res

    def get_self_node(self):
        service = self.name + '/supervisor/get_self'
        self.robot_node = self.call_service(service, get_uint64)

        return self.robot_node

    def get_self_position(self):
        service = self.name + '/supervisor/node/get_position'
        pos = self.call_service(service, node_get_position, self.robot_node.value)
        return pos

    def simulation_set_mode(self, mode):
        service = self.name + '/supervisor/supervisor_simulation_set_mode'

        res = self.call_service(service, set_int, mode)

        return res

    def simulation_get_mode(self):
        service = self.name + '/supervisor/supervisor_simulation_get_mode'

        res = self.call_service(service, get_int)

        return res

    def remove_node(self, node_id):
        service = self.name + '/supervisor/node/remove'

        res = self.call_service(service, node_remove, node_id)

        return res

    def remove_field(self, field_id):
        service = self.name + '/supervisor/field/remove'

        res = self.call_service(service, field_remove, field_id, -1)

        return res

class Field:
    
    # Types of ROS services to get a field coresponding to each Webots type
    WEBOTS_FIELDS_GET_TYPES = {
        'SFInt32': field_get_int32,
        'SFString' : field_get_string,
        'MFFloat': field_get_float,
        'SFBool': field_get_bool,
        'SFRotation': field_get_rotation,
        'SFVec3f': field_get_vec3f,
        'SFFloat': field_get_float,
        'MFNode': field_get_node
    }

    # Types of ROS services to set a field coresponding to each Webots type
    WEBOTS_FIELDS_SET_TYPES = {
        'SFInt32': field_set_int32,
        'SFString' : field_set_string,
        'MFFloat': field_set_float,
        'SFBool': field_set_bool,
        'SFRotation': field_set_rotation,
        'SFVec3f': field_set_vec3f,
        'SFFloat': field_set_float,
        'MFNode': field_import_node_from_string
    }

    # Webots types are written differently in the ROS services URIs.
    WEBOTS_TYPE_TO_ROS_SERVICE = {
        'SFInt32': 'int32',
        'SFString': 'string',
        'MFFloat': 'float',
        'SFBool': 'bool',
        'SFRotation': 'rotation',
        'SFFloat': 'float',
        'SFVec3f': 'vec3f',
        'MFNode': 'node'
    }

    def __init__(self, field_id, supervisor):
        self.id = field_id
        self.supervisor = supervisor

    def get_type(self):
        """ Returns the type of the field (str) """

        service = self.supervisor.name + '/supervisor/field/get_type_name'
        response = self.supervisor.call_service(service, field_get_type_name, self.id)

        return response.name

    def __getitem__(self, index):
        """ Allows the syntax value = field[i]. If the field is not an array,
        then use field[0] to read the value. """
        wb_type = self.get_type()
        
        url = self.WEBOTS_TYPE_TO_ROS_SERVICE[wb_type]

        service = '{}/supervisor/field/get_{}'
        service = service.format(self.supervisor.name, url)

        service_type = self.WEBOTS_FIELDS_GET_TYPES[wb_type]

        response = self.supervisor.call_service(service, service_type, self.id, index)

        return response.value

    def __setitem__(self, index, value):
        """ Allows the syntax field[i] = value. If the field is not an array,
        then use field[0] to write the value.
        
        Please note that you must provide a value of the correct ROS service type,
        e.g. from geometry_msgs.msg import Vector3, for a vec3 """
        wb_type = self.get_type()
        
        url = self.WEBOTS_TYPE_TO_ROS_SERVICE[wb_type]

        service = '{}/supervisor/field/set_{}'
        service = service.format(self.supervisor.name, url)

        service_type = self.WEBOTS_FIELDS_SET_TYPES[wb_type]

        self.supervisor.call_service(service, service_type, self.id, index, value)

class Node:
    def __init__(self, node_id, supervisor):
        
        self.id = node_id
        self.supervisor = supervisor

    def __getitem__(self, key):
        """ Enables the syntax node['field'] to read the value of a field.
        Throws a KeyError if the node has no field with this name.
        
        Args:
            key (str): name of the field to obtain.

        Returns:
            A Field object
        """

        # Obtain field ID from Webots ROS API
        service = self.supervisor.name + '/supervisor/node/get_field'
        response = self.supervisor.call_service(service, node_get_field, self.id, key, 0)
        field_id = response.field

        if field_id == 0:
            raise KeyError('No field named: {}'.format(key))

        field = Field(field_id, self.supervisor)

        return field

    def __setitem__(self, key, string_value):
        """ Enables the syntax node['field'] = string_value to set the value of a node field.
        e.g. robot_node['children'] = spine_node_string 
        Throws a KeyError if the node has no field with this name.
        
        Args:
            key (str): name of the field to set.
            string_value (str): a text string containing the node definintion.
            Look at .wbt files to check the syntax.
        """

        # Obtain field ID from Webots ROS API
        service = self.supervisor.name + '/supervisor/node/get_field'
        field_id = self.supervisor.call_service(service, node_get_field, self.id, key)

        if field_id == 0:
            raise KeyError('No field named: {}'.format(key))

        # Set the field from the given string
        service = self.supervisor.name + '/supervisor/field/import_node_from_string'
        self.supervisor.call_service(service, field_import_node_from_string, field_id, -1, string_value)

    def __delitem__(self, key):
        """ Enables the syntax del node['field'] to remove a field
        Throws a KeyError if the node has no field with this name.
        
        Args:
            key (str): name of the field to delete.
        """

        # Obtain field ID from Webots ROS API
        service = self.supervisor.name + '/supervisor/node/get_field'
        field_id = self.supervisor.call_service(service, node_get_field, self.id, key)

        if field_id == 0:
            raise KeyError('No field named: {}'.format(key))

        self.supervisor.remove_field(field_id)

