# -*- coding: utf-8 -*-
""" Run simulation on worlds containing patches, and record data to rosbags.

Example:
    TODO add


TODO:
    * complete this documentation

"""

import numpy as np
import os
import re
import time
import natsort
from tqdm import tqdm
from collections import Counter
import pdb
import random
import time
import logging

from tf import transformations

# For driving the Webots simulation
from utilities.webots2ros import Supervisor, Node
from .robot_manager import KrockManager, KrockState
from webots_ros.srv import field_set_float, field_set_vec3f
from geometry_msgs.msg import Vector3

# For triggering rosbag records
import subprocess, signal, shlex, psutil

file_logger = logging.getLogger("pipeline.general")


class SimulationManager:

    WORLD_LOAD_TIME = 10 #3.0
    FALL_STABILIZATION_TIME = 2.5 #1.5 
    STAND_UP_TIME = 1.0
    STOP_TIME = 1.5 #1.5
    RE_ORIENT_TIME = 0.5

    PATCH_NAME_STRING = '# patch_name "'
    FIELD_SPAWN_POSITION = '# spawn_position'

    SUBMAP_XSIZE = '# patch_size_x "'
    SUBMAP_ZSIZE = '# patch_size_z "'
    SUBMAP_RESOLUTION = '# resolution "'

    DEFAULT_FRICTION_DEF = 'CONTACT_DEFAULT_DEFAULT'
    FOOT_FRICTION_DEF = 'CONTACT_FOOT_DEFAULT'
    WORLD_INFO_DEF = 'world_info'

    #TARGET_OFFSET = 1.5 # distance in meters from the spawning (landing) pose to the target position
    FRICTION_HIGH = 1000
    DEFAULT_FRICTION = 0.25
    DEFAULT_FOOT_FRICTION = 0.5
    GRAVITY_LOW = -3.0
    GRAVITY_DEFAULT = -9.81
    

    def __init__(self, robot_name='krock2', ros_supervisor_name='ros_supervisor',
                 patch_simulation_duration=10.0, target_offset=1.5, runs_per_submap=1, fixed_poses=None, max_spawn_y=1.4):
        """ Constructor.

        Args:
            robot_name (str): Name of the robot, used for the ROS services to
            communicate with the Krock controller.
            ros_supervisor_name (str): Name of the robot with the `ros` controller,
            used to access the Webots supervisor API.
            patch_simulation_duration (float): Duration in seconds for each patch.
        """

        self.patch_simulation_duration = patch_simulation_duration

        self.TARGET_OFFSET= target_offset

        self.runs_per_submap = runs_per_submap # how many times will the robot be spawned and move on a submap

        self.fixed_poses=fixed_poses # use only when want to try specific poses in simulation (e.g., evaluation)

        self.max_spawn_y = max_spawn_y # depends on the scalling of the simulated submap, set it to the max
                                       # possible value x some extra

        # The supervisor is used to interact with webots
        self.supervisor = Supervisor(ros_supervisor_name)

        self.robot_manager = KrockManager(self.supervisor, robot_name)

        # To make sure to stop bags if necessary
        self.__recording_bag = False

        # Webots has different rotations/axis than ROS, thus quaternion order is changed
        # towards sumap's X (red) ?
        #qto = transformations.quaternion_from_euler(0, 0, 2*np.pi, axes='sxyz')
        #self.front_orientation = [qto[0], qto[2], qto[1], qto[3]]                

    def get_world_files(self, world_folder, world_prefix):
        """ Returns a list of paths of each world found.

        Args:
            world_folder (str): Folder in which to scan for world files (.wbt).
            world_prefix (str) : Name of the original patch subfolder, which will be use to
            recover only the worlds named <world_prefix>_world_<i>.wbt. If world_prefix
            is an empty string (''), the worlds fetched will be world_<i>.wbt

        Returns:
            A list of paths to webots worlds, sorted alphanumerically.
        """

        prefix = world_prefix
        if prefix == '':
            prefix = 'world'

        # Order worlds to run simulation in the correct order
        return natsort.natsorted([os.path.realpath(f.path) for f in os.scandir(world_folder) \
                                  if f.is_file() and f.name.split('_')[0] == prefix and f.name.endswith('.wbt')])

    def find_patches(self, world_path):
        """ Returns the list of tuple (name, [x,y,z]) for each patch contained in a world.

        Args:
            world_path (str): World in which to scan for patches.

        Returns:
            A list of tuples (name, [x,y,z]) with the name and spawn position of each patch.
        """

        world_string = ''

        # Read world file
        with open(world_path, 'r') as file:
            world_string = file.read()

        if not world_string:
            return []

        patches = []

        # Find each occurence of a patch name
        occurence = world_string.find(self.PATCH_NAME_STRING)
        while occurence != -1:

            # Find patch name
            name_begin = occurence + len(self.PATCH_NAME_STRING)

            name_end = world_string[name_begin:].find('"')
            if name_end != -1:
                name_end += name_begin

            patch_name = world_string[name_begin:name_end]

            # Find patch position
            translation_begin = world_string[occurence:].find(self.FIELD_SPAWN_POSITION)

            str_ipos_submap_xsize = world_string[occurence:].find(self.SUBMAP_XSIZE) 
            str_ipos_submap_zsize = world_string[occurence:].find(self.SUBMAP_ZSIZE) 
            str_epos_submap_xsize = world_string[occurence:][str_ipos_submap_xsize+len(self.SUBMAP_XSIZE):].find('"')
            str_epos_submap_zsize = world_string[occurence:][str_ipos_submap_zsize+len(self.SUBMAP_ZSIZE):].find('"')             
            submap_xsize = world_string[occurence:][str_ipos_submap_xsize+len(self.SUBMAP_XSIZE): str_ipos_submap_xsize+len(self.SUBMAP_XSIZE)+str_epos_submap_xsize]
            submap_zsize = world_string[occurence:][str_ipos_submap_zsize+len(self.SUBMAP_ZSIZE): str_ipos_submap_zsize+len(self.SUBMAP_ZSIZE)+str_epos_submap_zsize] 
            #print ("sub strings: ", submap_xsize, submap_zsize)

            str_ipos_submap_resolution = world_string[occurence:].find(self.SUBMAP_RESOLUTION) 
            str_epos_submap_resolution = world_string[occurence:][str_ipos_submap_resolution+len(self.SUBMAP_RESOLUTION):].find('"')
            submap_resolution = world_string[occurence:][str_ipos_submap_resolution+len(self.SUBMAP_RESOLUTION): str_ipos_submap_resolution+len(self.SUBMAP_RESOLUTION)+str_epos_submap_resolution]
            #print ("res: ", submap_resolution)

            if translation_begin != -1:
                translation_begin += occurence + len(self.FIELD_SPAWN_POSITION)

            # at most 100 characters to store 3 floats ?
            translation_end = translation_begin + 100

            # Find 3 signed float numbers with regex
            position = re.findall(r"[-+]?\d*\.\d+|\d+", world_string[translation_begin:translation_end])[:3]
            # str to float
            position = [float(x) for x in position]

            patches.append((patch_name, position, (int(submap_xsize), int(submap_zsize)), float(submap_resolution) ))

            # Find next occurence of a patch name
            occurence = world_string.find(self.PATCH_NAME_STRING, occurence + 1)

        return patches

    def start_recording(self, bag_name, topics):
        """ Start recording a rosbag containing the specified topics.

        Please note that only one bag at a time should be recorded. Hence
        calling this function several times without calls to stop_recording()
        will have no effect.

        Args:
            bag_name (str): The bag will be named <bag_name>.bag
            topics (list): List of topics (as strings) to record.
        """

        # Don't record a new bag if already recording one
        if self.__recording_bag:
            file_logger.error('A rosbag is already being recorded.')
            return

        # Build rosbag record command with all specified topics
        rosbag_record_command = 'rosbag record -O {}'.format(bag_name)
        
        for topic in topics:
            rosbag_record_command += ' ' + topic

        # The quiet (-q) option of rosbag record does not work... Hence we use
        # this trick to remove the console outputs
        fh = open("NUL","w")

        # Run command in a new subprocess
        rosbag_record_command = shlex.split(rosbag_record_command)
        self.__rosbag_proc = subprocess.Popen(rosbag_record_command, stdout=fh)
        fh.close()

        # Wait some time for the bag to start
        time.sleep(0.05)

        self.__recording_bag = True

    def stop_recording(self):
        """ Stops recording the rosbag. Obviously, this won't have any effect
        if no bag is being recorded.
        """
        if not self.__recording_bag:
            return
            
        process = psutil.Process(self.__rosbag_proc.pid)
        for sub_process in process.children(recursive=True):
            sub_process.send_signal(signal.SIGINT)
        self.__rosbag_proc.wait()  # Wait for children to terminate
        self.__rosbag_proc.terminate()

        # If the rosbag is not shutdown cleanly it might be saved as .active
        # To fix this, use the following bash commands:
        # rosbag reindex *.bag.active
        # rosbag fix *.bag.active repaired.bag 
        # TODO: add this in postprocessing pipeline

        self.__rosbag_proc = None
        self.__recording_bag = False

    def change_friction_coefficient(self, contact_def, coef):
        """ Change the friction coefficient between the robot foots and the terrain.

        Args:
            contact_def (str): Webots DEF of the contact for which the friction
            should be changed.
            coef (float): New friction coefficient. Should be positive.
        """

        if coef < 0:
            return
        #print (">>>")
        #print (contact_def)
        #print ("<<<")
        self.supervisor.get_node_from_def(contact_def)['coulombFriction'][0] = coef

    def change_gravity(self, gravity):
        """ Change the gravity (in y direction) in Webots.

        Args:
            gravity (float): Gravity in [m/s^2].
        """

        self.supervisor.get_node_from_def(self.WORLD_INFO_DEF)['gravity'][0] = Vector3(0, gravity, 0)

    def modify_physics_for_spawning(self):
        """ Increases the gravity and increases the friction, to allow soft spawning.
            The gravity is lowered for soft landing to avoid damaging the robot,
            while the friction is increased to make sure the robot does not slip. """

        self.change_friction_coefficient(self.DEFAULT_FRICTION_DEF, self.FRICTION_HIGH)
        self.change_friction_coefficient(self.FOOT_FRICTION_DEF, self.FRICTION_HIGH)
        self.change_gravity(self.GRAVITY_LOW)

    def revert_physics_default(self):
        """ Resets gravity and friction back to normal. """
        
        self.change_friction_coefficient(self.DEFAULT_FRICTION_DEF, self.DEFAULT_FRICTION)
        self.change_friction_coefficient(self.FOOT_FRICTION_DEF, self.DEFAULT_FOOT_FRICTION)
        self.change_gravity(self.GRAVITY_DEFAULT)

    def get_random_pose (self, submap_size, tolerance=1.0):
        """ Generate a random pose for the k-rock2 on a map of size (m).
            Use tolerance (m) to generate poses within submap_size - tolerance regions
        """
        y = self.max_spawn_y # 1.7 # fixed elevation for robot spawning (m)
        x = random.uniform(tolerance, submap_size[0]-tolerance)
        z = random.uniform(tolerance, submap_size[1]-tolerance)
        random_position = [x, y, z]
        yaw = random.uniform(-np.pi, np.pi)
        #qto = transformations.quaternion_from_euler(0, 0, yaw, axes='sxyz') # roll, pitch, yaw
        # NOTE: Webots does not use a quaternion convention to represent angles
        #
        qto = transformations.quaternion_from_euler(0, 0, yaw, axes='sxyz')
        random_orientation = [qto[0], qto[2], qto[1], qto[3]]

        #test unit
        #return [[8.860309049130069, 0.9, 2.79754154117016], [0.0, -0.9641994872801504, 0.0, 0.2651779567171735]]        

        return [random_position, random_orientation]
        


    def run_simulations(self, worlds_folder_path, bags_folder_path):
        """ Read an input file containing webpts worlds, run simulations and records data while
        the robot is traversing patches.

        Args:
            worlds_folder_path (str): Input folder containing webots worlds (.wbt), or subfolders
            which contain worlds. Only one level of subfolders will be checked for worlds.
            Please note that the specified folder must be accessible by webots, hence the path given
            here must be in the shared_folder so that the world can be read from within the docker
            environment and loaded by webots outside it !

            bags_folder_path (list): Output folder where the recorded data (rosbags) will be saved. 
            Each world will contain at most self.patches_per_world elevation grids. Subfolders will be
            created in the ouput folder which correspond to the sub folders in the input worlds folder
            and the bags within them will correspond to the patches in the worlds within the input sub 
            folders.
        """

        # Check if input folder exists
        if not os.path.isdir(worlds_folder_path):
            file_logger.error(f'The specified input worlds directory does not exist: {worlds_folder_path}')
            return

        # Determine subfolder structure from world names: they are named <original_patch_folder>_world_<i>.wbt
        worlds_prefixes = [f.name.split('_')[0] for f in os.scandir(worlds_folder_path) if f.is_file() and f.name.endswith('.wbt')]

        worlds_counter = Counter(worlds_prefixes)

        subfolders = [name for name in worlds_counter.keys() if name != 'world']

        # Sort the folders alphanumerically to run the simulation folder per folder in order
        subfolders = natsort.natsorted(subfolders)

        # Add root folder to list of folders in which to look for worlds
        subfolders.insert(0, '')

        folder_progress_bar = tqdm(total=len(subfolders), desc='Folders scanned')

        for folder in subfolders:

            output_sub_folder = os.path.join(bags_folder_path, folder)

            # Get worlds in folder
            world_files = self.get_world_files(worlds_folder_path, folder)

            if len(world_files) == 0:
                folder_progress_bar.update(1)
                continue # no world in subfolder

            # Create ouput folder recursively if it does not exist
            os.makedirs(os.path.realpath(output_sub_folder), exist_ok=True)
            world_progress_bar = tqdm(total=len(world_files), desc='Worlds in folder')
            
            for world in world_files:
                # Load world in webots
                self.supervisor.load_world(world)
                # pdb.set_trace()
                # Get list of patches and their names
                patches = self.find_patches(world)
                # pdb.set_trace()
                # Leave time for webots to load the simulation
                time.sleep(self.WORLD_LOAD_TIME)

                #patch_progress_bar = tqdm(total=len(patches), desc='Patches in world')

                for patch_name, position, submap_size, resolution in patches:
                    #print ("current submap size: ", submap_size)
                    # Set high friction to prevent robot from slipping
                    total_runs = range(self.runs_per_submap) 
                    if self.fixed_poses is not None:
                        total_runs = range(len(self.fixed_poses))

                    nruns_progress_bar = tqdm(total=len(total_runs), desc='Runs in a submap')
                    for nrun in total_runs:
                        # resetting the simulator creates a bug where the controller 
                        # publishes the same position no matter the movement of 
                        # the robot in the simulator
                        # However, we need a way to reset the simulation to force
                        # the spawning of the robot to take into account the random
                        # orientation that sometimes is ignored (keeps the previous one)
                        
                        #self.supervisor.reset_simulation()
                        
                        # TODO: Ask Matt how to fix this. 

                        
                        self.modify_physics_for_spawning()

                        # Spawn robot
                        # position[0] -= 1.0
                        #spawn_pose = [np.array(position), self.front_orientation]
                        spawn_pose = []
                        if self.fixed_poses is None:
                            spawn_pose = self.get_random_pose((submap_size[0]*resolution, submap_size[1]*resolution), tolerance=2.5)
                        else:
                            passed_pose = self.fixed_poses[nrun]
                            #print (passed_pose)
                            qto = transformations.quaternion_from_euler(0, 0, passed_pose[3], axes='sxyz')
                            passed_orientation = [qto[0], qto[2], qto[1], qto[3]]
                            spawn_pose = [[passed_pose[0], passed_pose[1], passed_pose[2]], passed_orientation]
                        
                        file_logger.info (f'〽 Random spawn pose: {spawn_pose} ')
                        self.robot_manager.spawn_robot(spawn_pose)

                        # Wait until robot falls on the patch
                        self.supervisor.wait_for_simulation_time(self.FALL_STABILIZATION_TIME)
                        #self.robot_manager.re_orient(self.front_orientation)
                        # pdb.set_trace()RE_ORIENT_TIME = 0.5
                        #self.supervisor.wait_for_simulation_time(self.RE_ORIENT_TIME)
                        
                        # normal gait
                        #self.robot_manager.set_state(KrockState.STANDING)
                        
                        #  NOT DO THIS UNTIL futher notice self.robot_manager.set_state(KrockState.WALKING)
                        
                        # high gait 
                        self.robot_manager.set_state(KrockState.HIGH_STEP)
                        
                        #self.robot_manager.set_position_target(position)
                        # target position is d meters from the spawn positon ( actually, landing pose)
                        # towards the current (landing) orientation
                        current_pose = self.robot_manager.get_robot_pose_sim()
                        #self.robot_manager.set_position_target(spawn_pose)

                        # Wait until robot is standing
                        self.supervisor.wait_for_simulation_time(self.STAND_UP_TIME)

                        # Start rosbag recording
                        self.start_recording(os.path.join(output_sub_folder, patch_name + "-" + str(nrun) + '.bag'), 
                                            ['/' + self.robot_manager.robot_name + '/' + self.robot_manager.TOPIC_POSITION,
                                            '/' + self.robot_manager.robot_name + '/' + self.robot_manager.TOPIC_JOINT_DATA])
                        # pdb.set_trace()
                        # Reset friction and gravity to default values
                        self.revert_physics_default()

                        # Give motion target
                        file_logger.info (f'current position: {[[current_pose[0].x, current_pose[0].y, current_pose[0].z]]}')
                        file_logger.info (f'current orientation: {[[current_pose[1].x, current_pose[1].z, current_pose[1].y, current_pose[1].w]]}')
                        
                        current_yaw = transformations.euler_from_quaternion([current_pose[1].x, current_pose[1].z, current_pose[1].y, current_pose[1].w], axes='sxyz')[2] # roll, pitch, yaw
                        file_logger.info (f'current yaw: {current_yaw}')
                        
                        target_position = np.array([current_pose[0].x + (self.TARGET_OFFSET*np.cos(current_yaw)), 
                                        current_pose[0].y, 
                                        current_pose[0].z + -(self.TARGET_OFFSET*np.sin(current_yaw))])
                        file_logger.info (f'target position: {target_position}')
                        
                        file_logger.info (f'distance intended to travel: {np.sqrt( (target_position[0] - current_pose[0].x )**2 + (target_position[2] - current_pose[0].z)**2 ) }')

                        self.robot_manager.set_position_target([target_position, [current_pose[1].x, current_pose[1].z, current_pose[1].y, current_pose[1].w]])
                        #self.robot_manager.set_position_target(np.array(position) + np.array([self.TARGET_OFFSET, 0.0, 0.0]))


                        # Wait during simulation
                        # TODO: stop conditions: timeout, out of patch

                        py_start_time = time.time()
                        # BEAWARE:  This is the only rule for stopping the simulation: simulation duration. 
                        #           If the robot reaches the target pose before the duration expires, the robot
                        #           will be standing still the rest of the time. This is more visible when walking
                        #           on flat patches.
                        self.supervisor.wait_for_simulation_time(self.patch_simulation_duration)
                        py_elapsed_time = time.time() - py_start_time
                        current_end_pose = self.robot_manager.get_robot_pose_sim()
                        
                        file_logger.info (f'distance really travelled: {np.sqrt((current_end_pose[0].x - current_pose[0].x )**2 + (current_end_pose[0].z - current_pose[0].z)**2  )}')
                        file_logger.info (f'Elapsed time (python) for simulation run (submap walking) {py_elapsed_time}')                        

                        # Stop rosbag
                        self.stop_recording()

                        # stop robot
                        self.robot_manager.set_state(KrockState.INITIAL)
                        self.supervisor.wait_for_simulation_time(self.STOP_TIME)
                        
                        nruns_progress_bar.update(1)
                    nruns_progress_bar.close()
                    #patch_progress_bar.update(1)
                #patch_progress_bar.close()
                world_progress_bar.update(1)
            #
            #break

            world_progress_bar.close()
            folder_progress_bar.update(1)

        folder_progress_bar.close() 

# For testing purposes
if __name__ == '__main__':

    manager = SimulationManager()

    #manager.run_simulations('../ok/tamre', '../ok/bags')
    #manager.supervisor.load_world('/data/havy/idsia-traversability/core/data_generation_pipeline/ok/tamre/world_0.wbt')
