# -*- coding: utf-8 -*-
""" Manages the robot in simulation: spawning, state changes, position targets...

Example:
    TODO add


TODO:
    * complete this documentation

"""

import numpy as np
import os
import re
import time
from enum import IntEnum

import rospy

# ROS message types
from std_msgs.msg import Int32, Float64
from geometry_msgs.msg import Point, Pose, Quaternion, Vector3
from tf import transformations

from utilities.webots2ros import Supervisor, Node
import pdb

class KrockState(IntEnum):
    """ Available states for the Krock robot. Pleaser refer to controller.hpp
    
    """
    INITIAL = 1
    WALKING = 2
    HIGH_STEP = 3
    STANDING = 4
    POSING = 5
    SWIMMING = 6
    SUIT = 7
    AMPHIBIOUS = 8
    CRAWLING = 9
    ANIMAL_AQUASTEP = 10
    NONPERIODIC = 11

class KrockManager:

    # Publish to these
    TOPIC_STATE = 'requestedState'
    TOPIC_GAIT = 'gait'
    TOPIC_POS_TARGET = 'positionTarget'
    TOPIC_POSE_TARGET = 'poseTarget'

    # Subscribe to these
    TOPIC_POSITION = 'pose_throttle'
    TOPIC_TOUCH_SENSORS = 'touch_sensors'
    TOPIC_TORQUES = TOPIC_TOUCH_SENSORS = 'torques_feedback'
    TOPIC_JOINT_DATA = 'jointMeasuredData_throttle'
    
    def __init__(self, supervisor, robot_name='krock2'):
        """ Constructor.

        Args:
            supervisor (Supervisor): Webots supervisor, to call webots ROS services.
            robot_name (str): Name of the robot, used for the ROS services to
            communicate with the Krock controller.
        """

        # The supervisor is used to interact with webots
        self.supervisor = supervisor
        self.robot_name = robot_name

        rospy.init_node("krock_manager")

        self.init_publishers()
        self.init_subscribers()

    def init_publishers(self):
        # pdb.set_trace()
        topic_state = '/' + self.robot_name + '/' + self.TOPIC_STATE
        topic_gait = '/' + self.robot_name + '/' + self.TOPIC_GAIT
        topic_pos_target = '/' + self.robot_name + '/' + self.TOPIC_POS_TARGET
        topic_pose_target = '/' + self.robot_name + '/' + self.TOPIC_POSE_TARGET

        self.publisher_state = rospy.Publisher(topic_state, Int32, queue_size=10)
        self.publisher_gait = rospy.Publisher(topic_gait, Float64, queue_size=10)
        self.publisher_pos_target = rospy.Publisher(topic_pos_target, Point, queue_size=10)
        self.publisher_pose_target = rospy.Publisher(topic_pose_target, Pose, queue_size=10)

    def init_subscribers(self):
        #topic_joints = '/' + self.robot_name + '/' + self.TOPIC_STATE
        #self.publisher_pos_target = rospy.Subscriber(topic_joints, Float64, queue_size=10)
        pass

    def spawn_robot(self, pose):
        """ Respawns the robot at the specified position and orientation.

        Args:
            pose (list): Pose ([[x,y,z], [qx,qy,qz,a]]) at which to spawn the robot.
        """

        position, orientation = pose

        robot_node = self.supervisor.get_node_from_def(self.robot_name)
        
        self.supervisor.reset_node_physics(robot_node.id)        
        
        # pdb.set_trace()
        robot_node['translation'][0] = Vector3(*position)

        # Webots does not use quaternion convention to represent orientations
        # It uses: [rx, ry, rz, angle]. Where r elements (normalized) indicate the vector 
        # over which the rotation of angle radians is applied. 
        # However, the implementation of the bridge (Supervisor.py) seems to take care of this
        # (however, I am not sure if it properly sets the values of the rotation field in webots).
        # This could be a source of issues.
        # In the mean time, we will generate a Quaternion and set it directly.
        # In our case we want a yaw rotation, i.e. over the ry unit vector.
        #robot_node['rotation'][0] = Quaternion(*orientation)        

        # I think the interface developped by Thomas take care of this direct assignation of 
        # Quaternion to webots rotation scheme

        # this recall to reset the node may help to force the setting of the orientation
        # that sometimes is ignored (maybe aa API bug)
        self.supervisor.reset_node_physics(robot_node.id)        

        robot_node['rotation'][0] = Quaternion(*orientation)                

        #print ("requested spawned rotation:", *orientation,  " executed: ", robot_node['rotation'][0])

    def get_robot_pose_sim(self):
        """ Get ther obot pose from webots """
        robot_node = self.supervisor.get_node_from_def(self.robot_name)
        current_position = robot_node['translation'][0]
        # this call uses the webots ROS bridge from Supervisor.py,it seems
        # to return a quaternion (however, it is not clear how)
        current_orientation = robot_node['rotation'][0]
        return [current_position, current_orientation]



    def re_orient(self, orientation):
        """ This function re-orients the robot so we ensure same initial orientation
            after landing

        Args:
            pose (list): Re-defined Orientation ([qx,qy,qz,a]).
        """

        robot_node = self.supervisor.get_node_from_def(self.robot_name)
        self.supervisor.reset_node_physics(robot_node.id)
        orientation[1] = 1e-16
        robot_node['rotation'][0] = Quaternion(*orientation)

    def set_gait(self, gait_height):
        """ Change the gait of the robot.
        
        Args:
            gait_height (float): 0.0 for high stance, 1.0 for low stance.
        """
        msg = Float64(data=np.clip(gait_height, 0.0, 1.0))
        self.publisher_gait.publish(msg)

    def set_state(self, newState):
        """ Change the state of the robot.
        
        Args:
            newState (KrockState): The desired state, which must be reachable
            from the current state according to the robot state machine.
        """
        msg = Int32(data=newState.value)
        self.publisher_state.publish(msg)
        
    def set_position_target(self, target):
        """ Publishes the target pose of the robot (in ROS frame)
        
        Args:
            target (list): [[x,y,z],[x,y,z,w]] position target for the robot (position in webots frame and quaternion in which frame?)
        """
        position = target[0]
        q = target[1]

        # Note: Although we send this over ros to the high-level controller, we use the 
        # webots frame (x vertical, z horizontal, y out). It seems the controller uses such frame too.
        # i.e., we send target position commands in webtos frame.
        msg_position = Point(x=position[0], y=position[1], z=position[2])

        msg_pose = Pose(position=Point(x=position[0], y=position[1], z=position[2]), orientation=Quaternion(x=q[0], y=q[1], z=q[2], w=q[3])) 
        
        self.publisher_pos_target.publish(msg_position) # read by the controller to execute the command
        self.publisher_pose_target.publish(msg_pose) # use to save the cmplete target pose

