# -*- coding: utf-8 -*-
""" A set of parameters used across all the pipeline
"""

class SimulationParameters:
    """ A set of parameters used across all the pipeline.

    The traversability patch are defined with a height map (i.e. 'an image')
    and the convention is that they are traversed from left to right.
    """

    def __init__(self, resolution=0.02, patch_shape=(65, 65), patch_padding=(15, 65, 15, 15), scale=None):
        """ Constructor.

        Args:
            resolution (float): size in [m] corresponding to 1 px in the height map.

            patch_shape (tuple): Size of the patches in px (width, length). Since the patch are
                crossed from left to right, width is the lateral dimension and length is the 
                forward direction. The corresponding size in [m] is (width * resolution, 
                length * resolution).

            patch_padding (tupple) : Extra padding around the patch (back, front, left, right).
                The total shape will be (patch_shape[0] + left + right, 
                                         patch_shape[1] + back + front).
        """
        self.resolution = resolution
        self.patch_shape = patch_shape
        self.patch_padding = patch_padding
        #if scale is not None:
        self.submaps_scale = scale
        #else:
        #self.submaps_scale = 0