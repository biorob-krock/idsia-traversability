# Data generation pipeline

The single patch simulation approach for traversability data generation has been implemented
to overcome some limitations of the previous approach. The pipeline can be run with the python
script `run_new_pipeline.py`:

```bash
# From the core folder
python3 run_new_pipeline.py 
```

The pipeline is summarized in the 
following images:

<img src="readme_images/new_pipeline.png" height="400" hspace="5"/> <img src="readme_images/cover_image.png" height="400" hspace="5"/> 

## Patch (submap) generation 

This module generate subamps (previously known as patches) as intermediate files (`hdf5`) that are then taken by the world generator to create webots maps.

The current main source to generate maps are images. We employ 16bit uint grayscale images where pixel values represent elevation in meters. As uint images range from 0 to 2^16 (or from 0 to 1), they can only represent 1 meter of elevation. We overcome this limitation in the world generation module (and in the simulation manager module) were we indicate an **scale** to map the values to higher elevations.

> Note: this module could be replaced and moved to world generation as it seems unnecesary.

Please refer to the [patch generation README](patch_generation/README.md) for more details.

## Webots worlds generation

Once submaps (called patches in the filesystem) are generated, they should be used to build Webots worlds that will later be simulated. We use a one submap per world convention. 


### Generated worlds folder structure

The `generate_worlds()` function will look for patches in the specified folder, as well
as in its subfolders (only at the first level).


## Patch (submap) simulation

Once the worlds are generated, the simulations can finally be ran. This steps is the longest of the pipeline.
Our python script interacts with Webots via ROS services, for instance to load worlds or spawn the robot. 
Please note that that Webots must be opened, with a world containing the Krock controller with ROS interface,
oterwise the pipeline will not be able to run.

During this steps, the worlds previously generated are opened each after the other, and each patch within them
will be simulated in the following steps:

1. The robot is spawned at a random pose on the simulated submap
    a. Select a gait
2. The robot is instructed to stand up and to start moving forward towards a target (based on a distance)
3. We start recording the trajectory of the robot in a rosbag.
4. The robot is left some time to traverse the patch.
5. We instruct the robot to go back to idle mode.
    a. We stop the recording of the rosbag and save it.

### Rosbag folder structure

The folder containing the rosbags will have the same structure as the patch folder, infered when reading the
Webots world filenames:

```
# Given this folder as input:
worlds
├── world_0.wbt // Submap in world file

# The output in the specified output folder will be:
bag_folder
├── world 
│   ├── world0.bag
│   ├── world1.bag
│   ├── world2.bag
│   ├── ..........

```

## Post-processing

> Post processing module has been deprecated and moved to the ML module (outside this repository)


## Known bugs

There are a few knowns bugs in the new data generation pipeline:
* Be sure that the images used for patch generation are `uint16` (use imagemagik: `identify` and `convert`).
* Webots related
    * Depending on the size of the map and the simulation hardware, webots may take longer to load the world. If this time exceeds the time allocated for waiting webots, the simulation manager may start recording before hand and the data will be corrupted. 
* Running script related
    * If a error arises regarding the `webots_ros`, be sure to source the workspace from where the `webots_ros` package can be located.

## Authors

* **Omar**
* **Thomas Havy** - Semester project (2019)
