#!/bin/bash

# Script to copy the relevant files of the Webots environment into the shared folder

# Usage: to copy the Webots environment at <shared_folder>/webots_environnment:
# > ./copy_webots_env webots_environment

# This script will copy a world folder with the grid texture in it, and a controller
# folder with krock-controller subfolder containing the executable and the config
# files (no need for the source code etc.).

# Check the number of arguments
if [ $# -ne 1 ]; then
    echo "Invalid number of arguments.\nUsage: ./copy_webots_env <target_folder> (target_folder will be located in <shared_folder>)"
else
    # Get absolute path of this script
    SCRIPT_FOLDER=$(realpath $0)
    SCRIPT_FOLDER=$(dirname $SCRIPT_FOLDER)

    # Prepare target directory
    TARGET_FOLDER=$TRAV_PIPELINE_SHARED_FOLDER/$1
    echo "Copying Webots environment at: $TARGET_FOLDER"

    # Create if folders don't exist
    mkdir -p $TARGET_FOLDER
    mkdir -p $TARGET_FOLDER/worlds/textures/
    mkdir -p $TARGET_FOLDER/controllers/

    # Copy grid texture
    cp $SCRIPT_FOLDER/webots_environment/worlds/textures/grid_simple.jpg $TARGET_FOLDER/worlds/textures/

    # Copy all controllers executables and configs
    CONTROLLERS_FOLDER=$SCRIPT_FOLDER/webots_environment/controllers

    for f in $CONTROLLERS_FOLDER/*; do
        if test -d "$f"; then
            CONTROLLER_NAME=$(basename $f)

            # Check if executable controller exists in this folder
            if test -f "$f/$CONTROLLER_NAME"; then
                echo "Copying $CONTROLLER_NAME into $TARGET_FOLDER/controllers"
                mkdir -p $TARGET_FOLDER/controllers/$CONTROLLER_NAME
                cp $f/$CONTROLLER_NAME $TARGET_FOLDER/controllers/$CONTROLLER_NAME

                # Copy config folder if it exists
                if test -d "$f/config"; then
                    echo "Copying $CONTROLLER_NAME config directory"
                    cp -r $f/config/ $TARGET_FOLDER/controllers/$CONTROLLER_NAME
                fi
            fi
        fi
    done

fi;