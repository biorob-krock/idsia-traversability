#!/usr/bin/python3
""" Create webots simulation worlds from a set of patches.

Example:
    TODO add

TODO:
    * complete this documentation
    * Remove hardcoded components in file (for instance 'patch' field in hdf5)

"""

import numpy as np
import os
import shutil # For copying files
import h5py
import natsort
import pdb

class WebotsWorldGenerator:

    # Template files for world generation
    # Here we have a Fluid template node that will be integrated with the height map to have amphibious regions 
    TEMPLATE_FOLDER = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'template_files')
    TEMPLATE_WEBOTS_WORLD = os.path.join(TEMPLATE_FOLDER, 'empty_world_robot.wbt')
    #TEMPLATE_WEBOTS_WORLD = os.path.join(TEMPLATE_FOLDER, 'empty_world_robot_mod_mass.wbt')
    TEMPLATE_ELEVATION_GRID = os.path.join(TEMPLATE_FOLDER, 'elevation_grid.txt')
    TEMPLATE_AQUATIC_REGION = os.path.join(TEMPLATE_FOLDER, 'fluid_node.txt') 

    # Spacing between two grids
    GRID_SPACING = 2.0

    # Fields to replace in template elevation grid
    FIELD_GRID_DEF = 'PATCH_NAME'
    HIDDEN_PATCH_SIZE_X = 'PATCH_SIZE_X'
    HIDDEN_PATCH_SIZE_Z = 'PATCH_SIZE_Z'
    HIDDEN_RESOLUTION = 'RESOLUTION'
    FIELD_TERRAIN_DEF = 'TERRAIN_NAME'
    FIELD_HEIGHT_MAP = 'HEIGHT_VALUES'
    FIELD_DIMENSION_X = 'DIMENSION_X'
    FIELD_DIMENSION_Z = 'DIMENSION_Z'
    FIELD_SPACING_X = 'SPACING_X'
    FIELD_SPACING_Z = 'SPACING_Z'
    FIELD_SPAWN_X = 'SPAWN_X'
    FIELD_SPAWN_Y = 'SPAWN_Y'
    FIELD_SPAWN_Z = 'SPAWN_Z'
    FIELD_X = 'T_X' # forward
    FIELD_Y = 'T_Y' # up
    FIELD_Z = 'T_Z' # right
    
    AQ_NAME = 'Aq_Reg'
    FLUID_X = 'T_X' # forward
    FLUID_Y = 'T_Y' # up
    FLUID_Z = 'T_Z' # right

    SPAWN_Y_OFFSET = 0.1
    
    def __init__(self, aquatic_regions, patches_per_world=1, submaps_scale=None):
        """ Constructor.

        Args:
            patches_per_world (int): Max number of patches built in one world. 
        """
        self.patches_per_world = patches_per_world
        if submaps_scale is not None:
            self.submaps_scale = int(submaps_scale)
        else:
            self.submaps_scale = 0

        self.aq_flag = aquatic_regions
        # Read template grid 
        self.grid_template_string = ''
        with open(self.TEMPLATE_ELEVATION_GRID, 'r') as grid_file:
            self.grid_template_string = grid_file.read()

        # Read water node
        self.water_string = ''
        with open(self.TEMPLATE_AQUATIC_REGION, 'r') as water_file:
            self.water_string = water_file.read()

    def get_patch_files(self, folder, patch_extension='hdf5'):
        """ Returns a list of tuple with the patch filename (without extension) and path for each patch found.

        Args:
            folder (str): Folder in which to scan for patch files.
            patch_extension (str): Extension of the patch files

        Returns:
            A list of tuple [(name, path), ...] with name sorted in alphanumeric order.
        """
        return natsort.natsorted([(os.path.splitext(f.name)[0], os.path.realpath(f.path)) for f in os.scandir(folder) \
                                 if f.is_file() and f.name.endswith('.' + patch_extension)], key=lambda x: x[0])

    def generate_elevation_grid(self, grid_def, translation, patch, resolution, spawn_position):
        """ Builds a string representing an elevation map in webots with the values replaced with a patch.

        Args:
            grid_def (str): Name of the grid (each grid should be named differently !).
            translation (tuple): (x,y,z) position of the grid
            patch (np.array): Patch to use in the elevation grid
            resolution (float): Spacing between each point.
            spawn_position (tuple): (i, j) indices on the patch where the robot should be spawned.

        Returns:
            A string that can be added in a webots file.
        """

        # [[1, 2], [3, 4]] --> '1, 2, 3, 4'
        # TODO: check order of flattening to make sure the correct maps are built
        #np_array_to_comma_separated_string = lambda array: str(list(np.rot90(array, k=-1).T.flatten())).replace('[', '').replace(']', '')
        # this rotation is a tweak to simulate the map in the proper orientation (from image)
        # it may be fixed if we see how the hd5f file is stored (opencv reading is correct, the chage appears to be in hdf5 or webots format)
        patch_string = np.array2string(np.rot90(np.rot90(np.rot90(patch))), formatter={'float_kind':lambda x: "%.4f" % x}, separator=', ',
                                              threshold=np.prod(patch.shape)).replace('[', '').replace(']', '')
        #patch_string = np_array_to_comma_separated_string(patch)

        # Copy template
        grid_string = '{}'.format(self.grid_template_string)
        
        # Replace template values
        grid_string = grid_string.replace(self.FIELD_GRID_DEF, grid_def)
        grid_string = grid_string.replace(self.HIDDEN_PATCH_SIZE_X, str(patch.shape[0]))
        grid_string = grid_string.replace(self.HIDDEN_PATCH_SIZE_Z, str(patch.shape[1]))
        grid_string = grid_string.replace(self.HIDDEN_RESOLUTION, str(resolution))
        grid_string = grid_string.replace(self.FIELD_TERRAIN_DEF, 'terrain_' + grid_def)
        grid_string = grid_string.replace(self.FIELD_HEIGHT_MAP, patch_string)
        
        grid_string = grid_string.replace(self.FIELD_DIMENSION_X, str(patch.shape[1]))
        grid_string = grid_string.replace(self.FIELD_DIMENSION_Z, str(patch.shape[0]))
        grid_string = grid_string.replace(self.FIELD_SPACING_X, str(resolution))
        grid_string = grid_string.replace(self.FIELD_SPACING_Z, str(resolution))
        
        grid_string = grid_string.replace(self.FIELD_X, str(translation[0]))
        grid_string = grid_string.replace(self.FIELD_Y, str(translation[1]))
        grid_string = grid_string.replace(self.FIELD_Z, str(translation[2]))

        spawn_translation = np.array([0.0, 0.0, 0.0])
        spawn_translation[0] = spawn_position[1] * resolution + translation[0]
        spawn_translation[1] = np.amax(patch) + translation[1] + self.SPAWN_Y_OFFSET
        spawn_translation[2] = spawn_position[0] * resolution + translation[2]

        grid_string = grid_string.replace(self.FIELD_SPAWN_X, str(spawn_translation[0]))
        grid_string = grid_string.replace(self.FIELD_SPAWN_Y, str(spawn_translation[1]))
        grid_string = grid_string.replace(self.FIELD_SPAWN_Z, str(spawn_translation[2]))

        # Aquatic regions should be passed to this function
        if self.aq_flag:
            #pdb.set_trace()
            water_string = '{}'.format(self.water_string)
            water_string = water_string.replace(self.AQ_NAME, 'Aq_Reg_' + grid_def)
            water_string = water_string.replace(self.FLUID_X, str(translation[0]+1.45))
            water_string = water_string.replace(self.FLUID_Y, str(translation[1]))
            water_string = water_string.replace(self.FLUID_Z, str(translation[2]+0.95))
            grid_string = grid_string + water_string # Here we add the fluid node

        return grid_string

    def create_world_from_patches(self, world_path, patches):
        """ Create a webots world with elevation grid built with the given patches.

        Args:
            world_path (str): Path of the webots world to create.
            patches (list): List of tuples (name, paths to hdf5 patches files). 
        """

        # Copy file from template
        res = shutil.copy2(self.TEMPLATE_WEBOTS_WORLD, world_path)

        with open(world_path, 'a') as world_file:
            offset = 0.0
            for name, file in patches:
                with h5py.File(file, 'r') as hdf5_file:
                    patch = hdf5_file['patch'][:]
                    resolution = hdf5_file['resolution'][()] # Weird h5py syntax for reading just a scalar
                    spawn_position = hdf5_file['spawn_position'][:]

                grid_string = self.generate_elevation_grid(name, (0.0, 0.0, offset), patch, resolution, spawn_position)

                # Update offset for next patch
                offset += patch.shape[0] * resolution + self.GRID_SPACING

                world_file.write(grid_string)

    def generate_worlds(self, patches_folder_path, worlds_folder_path):
        """ Read an input file containing patches, and creates webots worlds filled 
        with elevation grid created with these patches.

        Args:
            patches_folder_path (str): Input folder containing patch files (.hdf5), or subfolders
            which contain patch files. Only one level of subfolders will be checked for patches.
            worlds_folder_path (list): Output folder where the create worlds will be saved. Each
            world will contain at most self.patches_per_world elevation grids. Subfolders will be
            created in the ouput folder which correspond to the sub folders in the input patch folder
            and the world within them will correspond to the patches in the input sub folders.
        """

        # Check if input folder exists
        if not os.path.isdir(patches_folder_path):
            print('The specified input patch directory does not exist: {}'.format(patches_folder_path))
            return

        # Create ouput folder recursively if it does not exist
        os.makedirs(os.path.realpath(worlds_folder_path), exist_ok=True)

        # Find list of patches in input folder and subfolder (only one level of folder)
        subfolders = [(f.name, f.path) for f in os.scandir(patches_folder_path) if f.is_dir() ]

        # Add root patches_folder_path to list of folders in which to look for patches
        subfolders.insert(0, ('', patches_folder_path))

        for folder in subfolders:

            # Get patches in folder
            patch_files = self.get_patch_files(folder[1])

            if len(patch_files) == 0:
                continue # no patch in subfolder

            # Split into batches
            n_batches = int(np.ceil(len(patch_files) / self.patches_per_world))

            # Create worlds that contains self.patches_per_world patches: n_batches correspond number of worlds
            for i in range(n_batches):
                patches = patch_files[(i * self.patches_per_world):(((i + 1) * self.patches_per_world))]
                
                world_name = ''
                if folder[0] == '':
                    #world_name = 'world_' + str(i) + '.wbt'
                    world_name = 'world_' + str(int(self.submaps_scale)) + '.wbt'
                else:
                    #world_name = folder[0] + '_world_' + str(i) + '.wbt'
                    world_name = folder[0] + '_world_' + str(int(self.submaps_scale)) + '.wbt'
                
                self.create_world_from_patches(os.path.join(worlds_folder_path, world_name), patches)
        

# For testing purposes
if __name__ == '__main__':

    worldGenerator = WebotsWorldGenerator(patches_per_world=10)

    worldGenerator.generate_worlds('/data/havy/shared_folder/patches', '/data/havy/shared_folder/webots_environment/worlds')