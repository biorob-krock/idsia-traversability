# -*- coding: utf-8 -*-
""" Classes and util functions to generate patches.

For a simple use, you can use the ImagePatchGenerator to generate patches from 
an image.

If a kind of patch needs to be tested thoroughly by varying parameters, create a class
that is derived from PatchGenerator. Basically you need to override the get_parameters() 
function to specify the parameters that can be varied in your patches and the valid range 
for each of them. Then, you only need to implement the build_height_map() function to
define how the patches are generated.
Look at StepPatchGenerator for instance to check how to do so.

"""

import numpy as np
import abc # abstract class
import math
import random
import itertools # cartesian product
import h5py
import os
import pdb

from scipy.interpolate import interp1d

from PIL import Image # Basic image loading and processing

import cv2

class PatchGenerator(metaclass=abc.ABCMeta):
    """ Abstract base class for patch generators. Classes deriving from this must implement
    the abstract methods get_parameters() and build_height_map(). """

    def __init__(self, resolution, patch_shape, patch_padding):
        """ Constructor.

        Args:
            resolution (float): size in [m] corresponding to 1 px in the height map.

            patch_shape (tuple): Size of the patches in px (width, length). Since the patch are
                crossed from left to right, width is the lateral dimension and length is the 
                forward direction. The corresponding size in [m] is (width * resolution, 
                length * resolution).

            patch_padding (tupple) : Extra padding around the patch (back, front, left, right).
                The total shape will be (patch_shape[0] + left + right, 
                                         patch_shape[1] + back + front).
        """
        self.patch_shape = patch_shape
        self.patch_padding = patch_padding

        self.resolution = resolution
        self.rnd_map = 0

        # Center of patch where the robot should be spawned
        if patch_shape is not None:
            self.spawn_position = (patch_padding[2] + patch_shape[0] // 2, 
                                 patch_padding[0] + patch_shape[1] // 2)

    @classmethod
    @abc.abstractmethod
    def get_parameters(cls):
        """ Get the list of parameters that can be varied for a particular type of patches.

        Returns:
            dict: A dictionary with the parameters and their associated ranges. If an element
            is a tuple, it means that the specified values are discrete, if it is a list, it
            means that it is a range between the two values.
        """
        return {}

    def save_to_hdf5(self, patch, path):
        """ Save a patch in hdf5 format.

        Args:
            patch (np.array): Patch to save
            path (str): Filepath of the patch.

        Returns:
            True if successfully saved, False otherwise.
        """

        with h5py.File(path, 'w') as hdf5_file:
            hdf5_file.create_dataset('patch', data=patch)
            hdf5_file.create_dataset('resolution', data=self.resolution)
            hdf5_file.create_dataset('spawn_position', data=self.spawn_position)
            hdf5_file.create_dataset('patch_padding', data=self.patch_padding)

        return True

    @abc.abstractmethod
    def build_height_map(self, **kwargs):
        """ Build one height map with specified parameters.

        Args:
            The specified kwargs params must be present in the parameter list (obtained
            with the method get_parameters())
        
        Returns:
            np.Array: An array representing the height map. 
            In case of errors, it returns None
        """
        return

    def build_random_height_map(self, **kwargs):
        """ Build a height map with random parameters from the possible ranges.

        Please note that if the infinite values in the ranges will be replaced 
        by 1e6 and -1e6.

        Args:
            Optional narrower intervals or specific values for certain parameters can
            be specified. The parameters that are not specified will be chosen completely
            randomly from the valid ranges.
        
        Returns:
            np.Array: An array representing the height map. 
            In case of errors, it returns None
        """

        valid_params = self.get_parameters()

        random_params = {}
        
        for param, valid_values in valid_params.items(): 

            random_value = None
            subset = None
            sub_interval = None

            # Check if a subset was specified for a given parameter
            if param in kwargs:
                value = kwargs[param]

                if type(value) is list and len(value) == 2: # Range of values
                    sub_interval = value

                elif type(value) is tuple: # Discrete set of values
                    subset = value

                else: # We assume it is a single value
                    random_value = value

            # Get a random parameter
            if random_value is None:
            
                if type(valid_values) is list and len(valid_values) == 2: # Range of values

                    if sub_interval is None:
                        low = valid_values[0]
                        high = valid_values[1]
                    else:
                        low = sub_interval[0]
                        high = sub_interval[1]

                    if low == -math.inf:
                        low = -1e6

                    if high == math.inf:
                        high = 1e6

                    random_value = random.uniform(low, high)

                elif type(valid_values) is tuple: # Discrete set of values
                    if subset is None:
                        random_value = random.choice(valid_values)
                    else:
                        random_value = random.choice(subset)

            if random_value is not None:
                random_params[param] = random_value

        # check validity of random parameters
        if not self.check_valid_parameters(**random_params):
            return None
        else:
            return self.build_height_map(**random_params)

    def build_multiple_height_maps(self, **kwargs):
        """ Build one height map with specified parameters.

        Args:
            The specified kwargs params must be present in the parameter list (obtained
            with the method get_parameters()). For each parameter, the value must either
            be a single value or a list of values. All the combinations of parameters
            from the specified values will then be generated.
        
        Returns:
            list of np.Array: A list with arrays representing the height maps. 
            In case of errors, it returns None instead of each incorrect patch.
        """

        maps = []

        # Convert inputs to list (necessary for cartesian product)
        for param in kwargs:
            if type(kwargs[param]) is not list:
                try:
                    kwargs[param] = list(kwargs[param]) 
                except TypeError:
                    kwargs[param] = [kwargs[param]]

        # Build all combinations of parameters
        params_cartesian_product = (dict(zip(kwargs, x)) for x in itertools.product(*kwargs.values()))

        # generate one map per set of parameters
        for parameters in params_cartesian_product:
            maps.append(self.build_height_map(**parameters))

        return maps

    def check_valid_parameters(self, **kwargs):
        """ Check if the specified parameters/value pairs are valid, that is if the parameters
        exist and the specified value is in the allowed range/set.

        Returns:
            bool: True if all parameters/values are correct, False otherwise
        """

        valid_params = self.get_parameters()
        
        for param, value in kwargs.items(): 

            # Specified parameters is not in the list
            if not param in valid_params:
                return False
            
            valid_values = valid_params[param]

            if type(valid_values) is list and len(valid_values) == 2: # Range of values
                if value < valid_values[0] or value > valid_values[1]:
                    return False

            elif type(valid_values) is tuple: # Discrete set of values
                if not value in valid_values:
                    return False

            elif type(valid_values) is str: # Parameter is a string
                if valid_values == 'check exist':
                    if os.path.exists(value) == False:
                        return False
                else: 
                    return False

            else: # Incorrectly defined parameter
                return False

        return True

    def add_padding(self, patch):
        """ Add the patch padding around it.

        Args:
            patch (np.array): Input patch to be padded

        Returns:
            np.array: Padded patch
        """

        padding = max(self.patch_padding)

        # This will replicate the values on the edge of the patch
        if self.rnd_map == 1:
            padded_patch = np.pad(patch, padding, mode='constant', constant_values=(0.5,0.5))
        
        else:
            padded_patch = np.pad(patch, padding, mode='edge')

        # We need to trim some extra padding
        if self.patch_padding[0] < padding:
            extra_padding = padding - self.patch_padding[0]
            padded_patch = padded_patch[:, extra_padding:]
        if self.patch_padding[1] < padding:
            extra_padding = padding - self.patch_padding[1]
            padded_patch = padded_patch[:, :-extra_padding]
        if self.patch_padding[2] < padding:
            extra_padding = padding - self.patch_padding[2]
            padded_patch = padded_patch[extra_padding:, :]
        if self.patch_padding[3] < padding:
            extra_padding = padding - self.patch_padding[3]
            padded_patch = padded_patch[:-extra_padding, :]
        
        return padded_patch

class StepPatchGenerator(PatchGenerator):
    """ Class for generating step patches.
    
    Two parameters can be varied for this family of patches: the height of the step and its
    position along the left-to-right axis.
    """

    def __init__(self, resolution, patch_shape, patch_padding):
        super().__init__(resolution=resolution, patch_shape=patch_shape, patch_padding=patch_padding)

    @classmethod
    def get_parameters(cls):
        params = {
            'position': [0.0, 1.0], # relative position of the step (0: left of patch, 1: right of patch)
            'height': [-math.inf, math.inf] # Positive for a step up, and negative for a step down
        }

        return params

    def build_height_map(self, **kwargs):

        # check validity of input parameters
        if not self.check_valid_parameters(**kwargs):
            return None

        patch =  np.zeros(self.patch_shape)

        transition = 0
        if 'position' in kwargs: 
            transition = int(kwargs['position'] * self.patch_shape[1])

        height = 0
        if 'height' in kwargs: 
            height = kwargs['height']

        patch[:, transition:] = height

        patch = self.add_padding(patch)

        return patch

class SlopePatchGenerator(PatchGenerator):
    """ Class for generating slope patches.
    
    Two parameters can be specified for this family of patches: the final height 
    of the slope at the right side of the patch, and a rotation about the z axis.
    """

    def __init__(self, resolution, patch_shape, patch_padding):
        super().__init__(resolution=resolution, patch_shape=patch_shape, patch_padding=patch_padding)

    @classmethod
    def get_parameters(cls):
        params = {
            'final_height': [-math.inf, math.inf], # height at the right side of the map
            'rotation': [-np.pi/2, np.pi/2], # rotation about z axis (0: slope going towards right in patch, +pi/2 slope towards up, -pi/2 slope towards down)
            'texture': 'check exist', # Texture added on top of slope, defined by an image at the given path
            'texture_scale': [-math.inf, math.inf] # 255 on the texture image will be mapped to texture_scale
        }

        return params

    def build_height_map(self, **kwargs):
        """ Builds the slope patch. 
        
        """

        # check validity of input parameters
        if not self.check_valid_parameters(**kwargs):
            return None

        patch =  np.zeros(self.patch_shape)

        final_height = 0
        if 'final_height' in kwargs: 
            final_height = kwargs['final_height']

        rotation = 0
        if 'rotation' in kwargs: 
            rotation = kwargs['rotation']

        # Generate the plane

        # 3 points in the plane (if no rotation)
        p1 = np.array([0, 0, 0])
        p2 = np.array([0, patch.shape[0], 0])
        p3 = np.array([patch.shape[1], 0, final_height])

        # Rotate points about center of patch
        rotation_Matrix = np.array([[np.cos(rotation), -np.sin(rotation)], [np.sin(rotation), np.cos(rotation)]])
        center = np.array([patch.shape[1] / 2, patch.shape[0] / 2])

        p1[:2] = center + rotation_Matrix.dot(p1[:2] - center)
        p2[:2] = center + rotation_Matrix.dot(p2[:2] - center)
        p3[:2] = center + rotation_Matrix.dot(p3[:2] - center)

        # Define normal vector to the plane 
        v1 = p3 - p1
        v2 = p2 - p1

        # the cross product is a vector normal to the plane
        cp = np.cross(v1, v2)
        a, b, c = cp

        # This evaluates to d
        d = np.dot(cp, p1)

        # Generate patch
        x = np.arange(patch.shape[1])
        y = np.arange(patch.shape[0])
        X, Y = np.meshgrid(x, y)

        patch = (d - a * X - b * Y) / c

        # Apply texture
        if 'texture' in kwargs and 'texture_scale' in kwargs: 
            texture_path = kwargs['texture']
            texture_scale = kwargs['texture_scale']

            with Image.open(texture_path).convert('L') as img:

                # Scale image to patch dimensions
                img = img.resize(self.patch_shape, Image.ANTIALIAS)
        
                texture = np.array(img, dtype='f') * texture_scale / 255.0

                patch = patch + texture
            

        # Add padding
        patch = self.add_padding(patch)

        return patch

class StairsPatchGenerator(PatchGenerator):
    """ Class for generating stairs patches.
    
    Two parameters can be varied for this family of patches: the total height of the stairs
    and the number of steps in the stairs.
    """

    def __init__(self, resolution, patch_shape, patch_padding):
        super().__init__(resolution=resolution, patch_shape=patch_shape, patch_padding=patch_padding)

    @classmethod
    def get_parameters(cls):
        params = {
            'height': [-math.inf, math.inf], # Positive for stairs up, and negative for stairs down
            'steps': [1, math.inf] # Maximum actually depends on the resolution of the patches
        }

        return params

    def build_height_map(self, **kwargs):

        # check validity of input parameters
        if not self.check_valid_parameters(**kwargs):
            return None

        patch =  np.zeros(self.patch_shape)

        height = 0
        if 'height' in kwargs: 
            height = kwargs['height']

        steps = 1
        if 'steps' in kwargs: 
            steps = kwargs['steps']

        for x in range(patch.shape[1]):
            patch[:, x] = ((x / patch.shape[1]) // (1 / (steps + 1))) * height / steps

        patch = self.add_padding(patch)

        return patch

class ImagePatchGenerator(PatchGenerator):
    """ Class to generate patches from an image. The idea is to use an image as an heightmap
    and to scale it.    
    """
    
    def __init__(self, resolution, patch_shape, patch_padding, image_path):
        """ Constructor

        Args:
            resolution, patch_shape, patch_padding: See base class
            image_path (str): Filepath of the image to use.
        """

        super().__init__(resolution=resolution, patch_shape=patch_shape, patch_padding=patch_padding)

        # Load image
        with Image.open(image_path).convert('L') as img:

            self.raw_image = img.copy()

            # I should apply my changes from here
            # Scale image to patch dimensions
            img = img.resize(patch_shape, Image.ANTIALIAS)
    
            self.image = np.array(img, dtype='f')

    @classmethod
    def get_parameters(cls):
        params = {
            'offset': [-math.inf, math.inf], # Constant offset added on all the points of the patch
            'scale': [-math.inf, math.inf], # Scaling applied to the image, such that the values [0-255] are mapped to [0-scale]
            'rotation': [-180.0, 180.0] # Rotation in deg of the original image, note that if the rotation is not a multiple of 90, empty areas will appear
        }

        return params

    def build_height_map(self, **kwargs):
        """ Builds the image patch.        
        """
        pdb.set_trace()
        # check validity of input parameters
        if not self.check_valid_parameters(**kwargs):
            return None

        patch =  np.zeros(self.patch_shape)

        offset = 0.0
        if 'offset' in kwargs: 
            offset = kwargs['offset']

        scale = 1.0
        if 'scale' in kwargs: 
            scale = kwargs['scale']

        rotation = 0.0
        if 'rotation' in kwargs: 
            rotation = kwargs['rotation']

        if rotation == 0.0:
            patch = self.image * scale / 255.0 + offset
        else:
            # Rotate and scale image to patch dimensions
            img = self.raw_image.rotate(rotation, resample=Image.BILINEAR).resize(self.patch_shape, Image.ANTIALIAS)
            patch = np.array(img) * scale / 255.0 + offset
            
        

        patch = self.add_padding(patch)

        return patch

    def patch_gen(self):
        #pdb.set_trace()
        image = np.asarray(self.raw_image, dtype='uint8')
        ps0 = int(np.floor(image.shape[0]/self.patch_shape[0]))
        ps1 = int(np.floor(image.shape[1]/self.patch_shape[1]))
        # patches = np.zeros((self.patch_shape[0],self.patch_shape[1], ps0*ps1), dtype = 'uint8')
        maps = []
        #k = 0
        for i in range(ps0):
            for j in range(ps1):
                patch = image[i*self.patch_shape[0]:i*self.patch_shape[0] + self.patch_shape[0],\
                                 j*self.patch_shape[1]:j*self.patch_shape[1]+self.patch_shape[1]]
                self.rnd_map = 0
                maps.append(self.add_padding(patch/255))
                #patches[:,:,k] = self.add_padding(patch)
                #k += 1
                #f_name = 'patch_{}.jpg'.format(k)
                #io.imsave(f_name, patches[:,:,k-1])
                
        return maps

class ImageSubmapGenerator(PatchGenerator):
    """ Class to generate patches from an image. The idea is to use an image as an heightmap
    as is (in size); maybe to crop it. If scalling, be sure that the elevation values are kept.
    """
    
    def __init__(self, image_path, resolution=0.02, patch_shape=None, patch_padding=[0, 0, 0, 0], scale=None):
        """ Constructor

        Args:
            resolution, patch_shape, patch_padding: See base class
            image_path (str): Filepath of the image to use.
        """

        super().__init__(resolution=resolution, patch_shape=patch_shape, patch_padding=patch_padding)
        self.scale = scale

        # Load image (16 bit image that will be transformed to grayscale)
        img = cv2.imread(image_path, cv2.IMREAD_UNCHANGED )
        self.raw_image = img.copy()
        self.patch_shape = (self.raw_image.shape[0], self.raw_image.shape[1])

        patch_shape = self.patch_shape 
        # tentative spawning pose (why should we do this here?)
        self.spawn_position = (patch_padding[2] + patch_shape[0] // 2, 
                             patch_padding[0] + patch_shape[1] // 2)

        # I should apply my changes from here
        # Scale image to patch dimensions
        """
        if patch_shape is not None:
            print (f'Reshaping {patch_shape}')
            img = cv2.resize(self.raw_image, patch_shape, interpolation = cv2.INTER_AREA)
        else:
        """
        img = self.raw_image.copy()
        print (f'Original {image_path} image min {np.min(img)} max {np.max(img)} values')
        if self.scale is not None:
            rescale_img = interp1d( [np.min(img), np.max(img) ] , [0, self.scale])            
            img = rescale_img (img)
            print (f'->After rescaling to [0,{self.scale}] image min {np.min(img)} max {np.max(img)} values')
        else: 
            img = img / 2**16
            print (f'->No scale was passed.')

        #print (np.max(self.raw_image), np.min(self.raw_image), self.raw_image.shape)
        self.image = img.copy() # this is the array used for the world generation in webots
        

    @classmethod
    def get_parameters(cls):
        params = {
            'offset': [-math.inf, math.inf], # Constant offset added on all the points of the patch
            'scale': [0, math.inf], # Scaling applied to the image, such that the values [0-255] are mapped to [0-scale]
            'rotation': [-180.0, 180.0] # Rotation in deg of the original image, note that if the rotation is not a multiple of 90, empty areas will appear
        }

        return params
    
    def build_height_map(self, **kwargs):
        # Builds the image patch.                
        print ('Warning: I have been called \nWarning: I have been called\nWarning: I have been called')
        pdb.set_trace()
        # check validity of input parameters
        if not self.check_valid_parameters(**kwargs):
            return None

        patch =  np.zeros(self.patch_shape)

        offset = 0.0
        if 'offset' in kwargs: 
            offset = kwargs['offset']

        scale = 1.0
        if 'scale' in kwargs: 
            scale = kwargs['scale']

        rotation = 0.0
        if 'rotation' in kwargs: 
            rotation = kwargs['rotation']

        if rotation == 0.0:
            patch = self.image * scale / 255.0 + offset
        else:
            # Rotate and scale image to patch dimensions
            img = self.raw_image.rotate(rotation, resample=Image.BILINEAR).resize(self.patch_shape, Image.ANTIALIAS)
            patch = np.array(img) * scale / 255.0 + offset
            
        

        patch = self.add_padding(patch)

        return patch
    

    def patch_gen(self):
        #pdb.set_trace()
        #image = np.asarray(self.raw_image, dtype='uint8')
        image = self.image
        #ps0 = int(np.floor(image.shape[0]/self.patch_shape[0]))
        #ps1 = int(np.floor(image.shape[1]/self.patch_shape[1]))
        # patches = np.zeros((self.patch_shape[0],self.patch_shape[1], ps0*ps1), dtype = 'uint8')
        maps = []
        #k = 0
        maps.append(image)
        """
        for i in range(ps0):
            for j in range(ps1):
                patch = image[i*self.patch_shape[0]:i*self.patch_shape[0] + self.patch_shape[0],\
                                 j*self.patch_shape[1]:j*self.patch_shape[1]+self.patch_shape[1]]
                self.rnd_map = 0
                maps.append(self.add_padding(patch/255))
                #patches[:,:,k] = self.add_padding(patch)
                #k += 1
                #f_name = 'patch_{}.jpg'.format(k)
                #io.imsave(f_name, patches[:,:,k-1])
        """     
        return maps, self.raw_image // (2**16)


# For testing purposes
if __name__ == '__main__':

    patch_generator = SlopePatchGenerator(0.02, (65, 65), (15, 65, 15, 15))

    maps = patch_generator.build_multiple_height_maps(final_height=[1], rotation=[np.pi/4], texture=['/data/havy/shared_folder/patch_images/clubs1.png'], texture_scale=0.02)

    slopes_folder = '/data/havy/shared_folder/tests/'
    os.makedirs(slopes_folder, exist_ok=True)
    
    i=0
    for hm in maps:
        print(hm)
        patch_generator.save_to_hdf5(hm, os.path.join(slopes_folder, 'slope' + str(i) + '.hdf5'))
        i+=1
