# Patch generation

This module contains some utility classes to generate patch variations.

## Types of patch generators

Currently the following patch families can be generated:
* Step patches
* Slope patches, optionally with a texture applied on top
* Stairs patches
* Patches from images
* **TODO:** Create class to extract patches in high resolution images, such as the large maps
  generated with the previous pipeline. Indeed, it would be better for the data diversity to 
  randomly extract chunks from large images, rather than using always the same image with 
  different scalings.

## Generating patches

Here is an example on how to generate patches with the util classes, to generate slopes. 

```python
import data_generation_pipeline as dgp
import numpy as np
import os

# Generate slopes
slope_generator = dgp.patch_generation.SlopePatchGenerator(params.resolution, params.patch_shape, params.patch_padding)

# Generate slopes
maps = slope_generator.build_multiple_height_maps(final_height=np.linspace(-1, 1, num=5), rotation=np.linspace(-np.pi/2, np.pi/2, num=9))

# Save patches
slopes_folder = '/<path_to_shared_folder>/patches/slopes/'
os.makedirs(slopes_folder, exist_ok=True)

for i, hm in enumerate(maps):
    slope_generator.save_to_hdf5(hm, os.path.join(slopes_folder, 'slope' + str(i) + '.hdf5'))
```

Basically, all patch generator classes share the function `build_multiple_height_maps()`, but 
different arguments should be provided depending on the type of patches (please refer to the 
classes' documentation). 

Each argument is optional, but if it is specified, it is mandatory to provide a list even for 
only a scalar number (e.g. `final_height=[1]` instead of `final_height=1`). 

This function will then generate all the combinations of the values passed in the parameters. 
For instance, if the function is called with the following parameters:

```python
slope_generator.build_multiple_height_maps(final_height=[1, 2], rotation=[-np.pi/2, 0, np.pi/2])
```

6 slope patches will be generated with each time a `final_height` of 1 or 2, and a rotation of
0, pi/2 or -pi/2.

## Visualizing patches

The notebook `patch_visualization.ipynb` can be used to visualize the generated patches.

Please note that because of the limitations of 3D surface plots with matplotlib, the vertical
scale will not be correct. Thus this visualization should be used only for quick checks.

If you want to visualize properly the generated patches, then you can generate Webots worlds
and open them.

## Creating a new type of patch generator

You can easily create new patch generator classes, without having to write too much code.
Basically, the infrastructure functions for generating multiple variations of the patch 
are already implemented in the base `PatchGenerator` class, and only a couple of function
need to be overriden in your child class:

```python
# Your class must derive from PatchGenerator
class SomePatchGenerator(PatchGenerator):

    # You can add parameters or variables in the constructor if you need to
    def __init__(self, resolution, patch_shape, patch_padding):
        super().__init__(resolution=resolution, patch_shape=patch_shape, patch_padding=patch_padding)

    # Add the parameters that can be varied for the type of patch you are creating
    # Make sure to add the correct range of possible values
    @classmethod
    def get_parameters(cls):
        params = {
            # For instance
            'height': [-math.inf, math.inf], # Interval of all values
            'steps': [np.pi, 10] # Interval of values
            'terrain': ('smooth', 'rough', 'bumps') # Set of discrete values
            'filepath': 'check exist'  # Do this if you want to specify a parameter corresponding
            # to a file (e.g. an image), so that the existence of the provided file is checked automatically
        }

        return params

    # Implement the function to actually build a map
    def build_height_map(self, **kwargs):

        # check validity of input parameters
        if not self.check_valid_parameters(**kwargs):
            return None

        patch =  np.zeros(self.patch_shape)

        # Obtain the arguments provided to the function in kwarks
        height = 0
        if 'height' in kwargs: 
            height = kwargs['height']

        steps = 1
        if 'steps' in kwargs: 
            steps = kwargs['steps']

        # Actually build the patch
        # ...
        # patch = 
        # ...

        # Add the padding around the patch for the simulations
        patch = self.add_padding(patch)

        return patch
```