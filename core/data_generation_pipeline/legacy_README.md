# New data generation pipeline

The single patch simulation approach for traversability data generation has been implemented
to overcome some limitations of the previous approach. The pipeline can be run with the python
script `run_new_pipeline.py`:

```bash
# From the core folder
python3 run_new_pipeline.py 
```

The pipeline is summarized in the 
following images:

<img src="readme_images/new_pipeline.png" height="400" hspace="5"/> <img src="readme_images/cover_image.png" height="400" hspace="5"/> 

## Patch generation

During this steps, the patch which need to be simulated are generated. The `patch_generation` 
folder contains some util classes to generate patch variations.

### Types of patch generators

Currently the following patch families can be generated:
* Step patches
* Slope patches, optionally with a texture applied on top
* Stairs patches
* Patches from images
* **TODO:** Create class to extract patches in high resolution images, such as the large maps
  generated with the previous pipeline. Indeed, it would be better for the data diversity to 
  randomly extract chunks from large images, rather than using always the same image with 
  different scalings.

Please refer to the [patch generation README](patch_generation/README.md) for more details.

## Webots worlds generation

Once patches are generated, they should be used to build Webots world that will later be simulated.

Each Webots world will contain several elevation grid, corresponding to the patches generated in 
the previous step of the pipeline. For loading time issues, it is better to have multiple patches
into one Webots world (we recommand 5-10 patches per world).

To generate worlds, use the following code:

```python
import data_generation_pipeline as dgp

world_folder = '/<path_to_shared_folder>/webots_environment/worlds'

worldGenerator = dgp.WebotsWorldGenerator(patches_per_world=10)
worldGenerator.generate_worlds(patch_folder, world_folder)
```

Please note that Webots requires a specific folder structure to run the simulation:
```
shared_folder
├── webots_environment
│   ├── controllers // The robot controller with ROS support should be in there
│   ├── worlds // The worlds should be generated in here
│   │   ├── textures
│   │   │   ├── grid_simple.jpg // To have a nice grid on the patches
```

### Generated worlds folder structure

The `generate_worlds()` function will look for patches in the specified folder, as well
as in its subfolders (only at the first level). The generated worlds follow this convention:

```
# Given this folder as input:
patch_folder
├── patch0.hdf5
├── patch1.hdf5
├── patch2.hdf5
├── ...........
├── slopes 
│   ├── slope0.hdf5
│   ├── slope1.hdf5
│   ├── slope2.hdf5
│   ├── ...........
├── steps 
│   ├── step0.hdf5
│   ├── step1.hdf5
│   ├── step2.hdf5
│   ├── ...........

# The output in the specified output folder will be:
worlds
├── world_0.wbt // First n patches in input root folder
├── world_1.wbt // Next n patches in input root folder
├── ........... // Rest of patches in input root folder
├── slopes_world_0.wbt // First n patches in slopes subfolder
├── slopes_world_1.wbt // Next n patches in slopes subfolder
├── .................. // Rest of patches in slopes subfolder
├── steps_world_0.wbt // First n patches in steps subfolder
├── steps_world_1.wbt // Next n patches in steps subfolder
├── ................. // Rest of patches in steps subfolder
```

## Patch simulations

Once the worlds are generated, the simulations can finally be ran. This steps is the longest of the pipeline.
Our python script interacts with Webots via ROS services, for instance to load worlds or spawn the robot. 
Please note that that Webots must be opened, with a world containing the Krock controller with ROS interface,
oterwise the pipeline will not be able to run.

During this steps, the worlds previously generated are opened each after the other, and each patch within them
will be simulated in the following steps:

1. The robot is spawned on a new patch
2. We start recording the trajectory of the robot in a rosbag.
3. The robot is instructed to stand up and to start walking forward towards a target in the distance, so that
it will traverse the patch.
4. The robot is left some time to traverse the patch.
5. We instruct the robot to go back to idle mode.
6. We stop the recording of the rosbag and save it.

This is done with the following code:

```python
import data_generation_pipeline as dgp

bags_folder = '/<path_to_shared_folder>/bags/'

manager = dgp.SimulationManager(robot_name='krock', patch_simulation_duration=10.0)
manager.run_simulations(world_folder, bags_folder)
```

### Rosbag folder structure

The folder containing the rosbags will have the same structure as the patch folder, infered when reading the
Webots world filenames:

```
# Given this folder as input:
worlds
├── world_0.wbt // First n patches in input root folder
├── world_1.wbt // Next n patches in input root folder
├── ........... // Rest of patches in input root folder
├── slopes_world_0.wbt // First n patches in slopes subfolder
├── slopes_world_1.wbt // Next n patches in slopes subfolder
├── .................. // Rest of patches in slopes subfolder
├── steps_world_0.wbt // First n patches in steps subfolder
├── steps_world_1.wbt // Next n patches in steps subfolder
├── ................. // Rest of patches in steps subfolder

# The output in the specified output folder will be:
bag_folder
├── patch0.bag
├── patch1.bag
├── patch2.bag
├── ..........
├── slopes 
│   ├── slope0.bag
│   ├── slope1.bag
│   ├── slope2.bag
│   ├── ..........
├── steps 
│   ├── step0.bag
│   ├── step1.bag
│   ├── step2.bag
│   ├── .........

```

## Post-processing

After all the simulations are ran, the trajectory of the robot for each simulated patch will be recorded in a
rosbag. Each rosbag is analyzed to determine which patches were successfully traversed. 

Internally, the rosbags are read and converted to Pandas dataframes for processing. For each trajectory (each patch)
some useful metrics will be extracted.

At the end of the pipeline, the result is a set of patches (the patches generated in the first step, which are numpy 
arrays saved as HDF5) and a dataframe of the following form:

| `patch` | `advancement` | `inital_position` | `final_position` |
|---------|---------------|-------------------|------------------|
| slope0  | 1.2           | [0, 0, 0]         | [1.2, 0.2, 0.1]  |
| slope1  | 1.05          | [0, 0, 2]         | [1.05, 0.2, 2]   |
| ......  | ...           | .........         | ...............  |
| step0   | 0.4           | [0, 0, 0]         | [0.4, 0.1, 0.3]  |
| .....   | ...           | .........         | ...............  |

Currently, for assigning the labels traversable or not, a simple threshold on the advancement column is used.
**TODO: it would be interesting to improve the post-processing to use more elaborate rules for assigning the labels** 

### Simulation results folder structure

Similarly to the previous parts of the pipeline, 

```
# Given this folder as input:
bag_folder
├── patch0.bag
├── patch1.bag
├── patch2.bag
├── ..........
├── slopes 
│   ├── slope0.bag
│   ├── slope1.bag
│   ├── slope2.bag
│   ├── ..........
├── steps 
│   ├── step0.bag
│   ├── step1.bag
│   ├── step2.bag
│   ├── .........

# The output in the specified output folder will be:
simulation_results
├── simulation_results.hdf5 // Results of patches in root patch folder
├── ..........
├── slopes 
│   ├── simulation_results.hdf5 // Results of patches in slopes subfolder
├── steps 
│   ├── simulation_results.hdf5 // Results of patches in steps subfolder
```

## How to use the generated dataset

At the end, you have a folder of generated patches, and a folder containing a summary dataframe 
(`simulation_results.hdf5`) for each subfolder. Each patch will have some associated simulation
results.

You can use the following code to read the results.

```python
import numpy as np
import pandas as pd
import h5py

# Read results
results_df = pd.read_hdf('/<path_to_shared_folder>/simulation_results.hdf5', 'df')
print(results_df.head(5))

# Read a patch
def load_hdf5_patch(path):
    with h5py.File(path, 'r') as hdf5_file:
        patch = hdf5_file['patch'][:]
        padding = hdf5_file['patch_padding'][:]
    
    # Remove padding for simulation    
    patch = patch[padding[2]:-padding[3], padding[0]:-padding[1]]

    return patch

patch = load_hdf5_patch('patches/step49.hdf5')

print(patch)
```

## Known bugs

There are a few knowns bugs in the new data generation pipeline:
* Patch generation
    * When generating patch from image (using the class ImagePatchGenerator) the rotations of the images are not correctly handled.
    When rotating the image with PIL, black pixels are added on the corner of the image to fill the missing pixels after rotation.
    In the generated patches, this results in steep steps either positive or negative. It would be a better idea to rotate the image
    and keep only a rectangle fully inscribed inside the rotated image to avoid the weird steps at the corner of the patches.
* Webots related
    * Infamous "exploded" robot bug. After some simulation time, the joints of the robot are sometimes disarticulated. There is no
    proper solution to this, only hacks. Francesco had solved the problem by periodically reloading the `children` node of the robot 
    in Webots.
    * Weird behavior on edge of elevation grid. The robot is sometimes thrust into the air instead of jump falling off.
    * Krock's tail sometimes gets stuck under the robot, sometimes preventing it from walking correctly.
    * Robot bouncing on the patch when spawned. This could be solved by limiting the speed of the robot when spawning it.
    * Robot spawned with non-zero velocity. Sometimes the robot is spawn with some momentum, this should not be the case.

## Authors

* **Thomas Havy** - Semester project
