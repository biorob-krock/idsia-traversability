#!/usr/bin/python3

import numpy as np
import os
import sys
import natsort
import data_generation_pipeline as dgp
import pdb
import cv2
import logging
import random


#params = dgp.SimulationParameters(resolution=0.02, patch_shape=(150, 150), patch_padding=(15, 15, 15, 15))
params = dgp.SimulationParameters(resolution=0.02, 
                                  patch_shape=None, 
                                  patch_padding=(0, 0, 0, 0), 
                                  scale=23)

# Get shared folder path from environment variable
shared_folder = os.environ.get('TRAV_PIPELINE_SHARED_FOLDER')
if shared_folder is None:
    sys.exit('Error: the environment variable for the shared folder was not set.')

# Use these flags to run only parts of the pipeline
run_patch_generation = False
run_world_generation = False
run_simulation = True
# post_processing module has been deprecated:
#   data tests have been moved to the ML pipeline (data verification)
#   data tests were unrliable on webots side, future simulation
#   controllers can allow to move back the data test in post_processing
#   module back to the simulation pipeline.
#   post_processing modules have been deleted
#run_post_processing = False
# a flag that indicates that the goal is to generate amphibious patches or not
aquatic_regions = False

# Run pipeline on very few samples, just to test if working
run_dummy_test = False

# Patch generation
patch_folder = os.path.join(shared_folder, 'patches/')
textures_folder = os.path.join(shared_folder, 'patch_textures/') # Applied to slopes
images_folder = os.path.join(shared_folder, 'patch_images/')

# World generation
world_folder = os.path.join(shared_folder, 'webots_environment/worlds/')

# Simulation
bags_folder = os.path.join(shared_folder, 'bags/')

# This module requieres two parameters:
# 1. the time the simulation will run from landing pose towards the target position
# 2. the intended distance the robot will travel from landing pose, it is used to compute
#    the targe position (keeping the landing orientation)
# If the distance is met (~ < 0.25 m) before the time, the robot will stop. If the time is spent before
# the robot reaches the distance, the data saving will end.
SIMULATION_TIME_PER_SUBMAP = 10 # seconds 8
SIMULATION_DISTANCE_OFFSET = 2.0 # meters 1.5
SIMULATION_RUNS_PER_SUBMAP = 5 # times the robot spawns and run on a submap

# Post-processing
results_folder = os.path.join(shared_folder, 'simulation_results/')

file_logger = logging.getLogger('pipeline.general')
file_logger.setLevel(logging.DEBUG)

handler = logging.FileHandler('log.log')

formatter = logging.Formatter('%(asctime)s  %(name)s %(levelname)s: %(message)s')
handler.setFormatter(formatter)

file_logger.addHandler(handler)


#file_logger = logging.getLogger('pipeline.general')
file_logger.info(f'Starting new data gathering (from simulation) experiment: time per submap {SIMULATION_TIME_PER_SUBMAP} , distance offset {SIMULATION_DISTANCE_OFFSET}, runs per submap {SIMULATION_RUNS_PER_SUBMAP}')

if run_dummy_test:

    print('Running dummy pipeline. To run full pipeline, set run_dummy_test to False in the script.')

    print('Generating patches...')
    slope_generator = dgp.patch_generation.SlopePatchGenerator(params.resolution, params.patch_shape, params.patch_padding)
    maps = slope_generator.build_multiple_height_maps(final_height=np.linspace(-1, 1, num=10))

    slopes_folder = os.path.join(patch_folder, 'slopes/')
    os.makedirs(slopes_folder, exist_ok=True)

    for i, hm in enumerate(maps):
        slope_generator.save_to_hdf5(hm, os.path.join(slopes_folder, 'slope' + str(i) + '.hdf5'))

    print('Building worlds...')
    os.makedirs(world_folder, exist_ok=True)
    worldGenerator = dgp.WebotsWorldGenerator(patches_per_world=5)
    worldGenerator.generate_worlds(patch_folder, world_folder)

    print('Running simulations... Make sure Webots is running and a world with the ROS controller is loaded.')
    print('(For instance, open \'core/data_generation_pipeline/world_generation/template_files/webots_environment/worlds/test_world.wbt\')')
    os.makedirs(bags_folder, exist_ok=True)
    manager = dgp.SimulationManager(robot_name='krock2', ros_supervisor_name='ros_supervisor', patch_simulation_duration=6.0)
    manager.run_simulations(world_folder, bags_folder)

    #print('Post-processing trajectories...')
    #os.makedirs(results_folder, exist_ok=True)
    #analyzer = dgp.TrajectoryAnalyzer()
    #analyzer.extract_results(bags_folder, results_folder)

    #sys.exit("Finished running dummy pipeline. To run full pipeline, set run_dummy_test to False in the script.")


# Pipeline modules begin:

# Patch generation ----------------------------------------------------------------------------------------------
if run_patch_generation:

    print('Generating patches...')
    
    #
    # Note: No basic shapes will be generated
    #
    # we will use images as elevation data (unless the Perlin noise generation is added?)

    # Generate patches (hd5f) from image patches (png)
    # Get (name, path) of all images important part to be changed. generating patches based on images
    # For now we don't need pure steps, stairs or slopes. We want to build height maps from an image
    # the approach is that we crop patches from a big height map and generate multiple patches each of which with random values from SimplexNoise
    
    images = natsort.natsorted([(os.path.splitext(f.name)[0], os.path.realpath(f.path)) for f in os.scandir(images_folder) \
                                    if f.is_file() and (f.name.endswith('.png') or f.name.endswith('.jpg') or f.name.endswith('.jpeg'))],
                                    key=lambda x: x[0])
    #pdb.set_trace()
    for image_name, image_path in images:

        #image_generator = dgp.patch_generation.ImagePatchGenerator(params.resolution, params.patch_shape, params.patch_padding, image_path)
        image_generator = dgp.patch_generation.ImageSubmapGenerator(image_path, params.resolution, params.patch_shape, params.patch_padding, params.submaps_scale)
        #pdb.set_trace()
        # I should change the definition and probably use a different function from build_multiple_height_maps
        # build_height_map should be changed/replaced with the new patch generation technique
        # maps = image_generator.build_multiple_height_maps(scale=np.linspace(-0.4, 0.4, num=10), rotation=np.linspace(-180, 180, 16, endpoint=False))
        maps, original_imgs = image_generator.patch_gen()
        #pdb.set_trace()
        image_folder = os.path.join(patch_folder, image_name)
        os.makedirs(image_folder, exist_ok=True)

        for i, hm in enumerate(maps):
            # NOTE: We generate 1 patch per map (the map itsself), so we will use the naming (postfix) to 
            #       indicate the scale instead of the patch number
            #image_generator.save_to_hdf5(hm, os.path.join(image_folder, image_name + '_' + str(i) + '.hdf5'))
            
            if params.submaps_scale is None:
                scale_postfix = 0
            else:
                scale_postfix = params.submaps_scale
            image_generator.save_to_hdf5(hm, os.path.join(image_folder, image_name + '_' + str(scale_postfix) + '.hdf5'))
            # pdb.set_trace()
            # careful when writing the treated image, be aware that the depth of the image (and the type) could have changed
            # and so, multiplying by 255 (without checking) could render undesired results
            ###cv2.imwrite(os.path.join(image_folder, image_name + '_' + str(i) + '.png'), hm*255)
            
            #cv2.imwrite(os.path.join(image_folder, image_name + '_' + str(i) + '.png'), (hm * (2**16)).astype(np.uint16) )
        
        # there is no need to re write these files, as the original maps (pngs) were used to create them
        #for i, org_hm in enumerate(original_imgs):
        #    cv2.imwrite(os.path.join(image_folder, image_name + '_' + str(i) + '.png'), org_hm.astype(np.uint16) )

    print('Done generating patches.')

# World generation ----------------------------------------------------------------------------------------
if run_world_generation:

    print('Building worlds...')
    # pdb.set_trace()
    # The robot is 'broken' (yeah Webots...) after a few landings
    #worldGenerator = dgp.WebotsWorldGenerator(aquatic_regions, patches_per_world=10)
    worldGenerator = dgp.WebotsWorldGenerator(aquatic_regions, patches_per_world=1, submaps_scale=params.submaps_scale)
    #pdb.set_trace()
    worldGenerator.generate_worlds(patch_folder, world_folder)

    print('Done building worlds.')

# Simulation ---------------------------------------------------------- NORMAL
"""
if run_simulation:
    fixed_poses = None
    # if we send yaw = 0, there is a NaN exception in webots (coming form Thomas' transformation from quaternion to webots rotation)
    #fixed_poses = [[2.5, 1.0, 1.2, 0.001], [4.7, 1.0, 1.2, 0.001],
    #               [4.7, 1.0, 4.5, 0.001],[4.7, 1.0, 7.0, 0.001],
    #               [6.5, 1.0, 7.0, np.pi/2],] #use when want to try fixed poses in simulation [x,y,z,yaw]
    #fixed_poses = [ [5.0, 1.0, 2.8, -np.pi/2], [4.2, 1.0, 5.2, 0.0001], [4.2, 1.0, 5.1, np.pi/2], [8.8, 1.0, 5.1, np.pi] ] #

    print('Running simulations... Make sure Webots is running and a world with the ROS controller is loaded.')
    print('(For instance, open \'core/data_generation_pipeline/world_generation/template_files/webots_environment/worlds/test_world.wbt\')')

    if params.submaps_scale is None:
        max_spawn_y = 1.4
    else:
        max_spawn_y = params.submaps_scale + 0.4
    print (f'Spawn y value (UP) set to {max_spawn_y}m')

    manager = dgp.SimulationManager(robot_name='krock2', ros_supervisor_name='ros_supervisor', 
                                    patch_simulation_duration=SIMULATION_TIME_PER_SUBMAP, 
                                    runs_per_submap=SIMULATION_RUNS_PER_SUBMAP,
                                    target_offset=SIMULATION_DISTANCE_OFFSET,
                                    fixed_poses=fixed_poses,
                                    max_spawn_y=max_spawn_y,
                                    )
    manager.run_simulations(world_folder, bags_folder)

    print('Done running simulations.')
"""

# Simulation ---------------------------------------------------------- predefined random poses
if run_simulation:
    fixed_poses = None
    # if we send yaw = 0, there is a NaN exception in webots (coming form Thomas' transformation from quaternion to webots rotation)
    #fixed_poses = [[2.5, 1.0, 1.2, 0.001], [4.7, 1.0, 1.2, 0.001],
    #               [4.7, 1.0, 4.5, 0.001],[4.7, 1.0, 7.0, 0.001],
    #               [6.5, 1.0, 7.0, np.pi/2],] #use when want to try fixed poses in simulation [x,y,z,yaw]
    #fixed_poses = [ [5.0, 1.0, 2.8, -np.pi/2], [4.2, 1.0, 5.2, 0.0001], [4.2, 1.0, 5.1, np.pi/2], [8.8, 1.0, 5.1, np.pi] ] #
    
    # generate N random poses for the Aletsch test map
    # This way will allow to know beforehand the random poses and re use them
    # on several simulation setups (e.g., different gaits)
    """
    num_random_poses = 400
    fixed_poses = []
    for i in range(num_random_poses):
        y = params.submaps_scale + 0.5 # 1.7 # fixed elevation for robot spawning (m)
        x = random.uniform(3.5, 2048*params.resolution)
        z = random.uniform(3.5, 2048*params.resolution)
        random_position = [x, y, z]
        yaw = random.uniform(-np.pi, np.pi)
        fixed_poses.append([x, y,z, yaw])
    with open("/home/omar/Codes/idsia-traversability/shared_folder/random_poses_aletsch.npy", 'wb') as f:
        np.save(f, np.array(fixed_poses))
    """
    
    fixed_poses = np.load("/home/omar/Codes/idsia-traversability/shared_folder/random_poses_aletsch.npy", allow_pickle=True).tolist()
    print(f'Loaded random poses {len(fixed_poses)}')
    #print (fixed_poses)


    print('Running simulations with fixed random poses... Make sure Webots is running and a world with the ROS controller is loaded.')
    print('(For instance, open \'core/data_generation_pipeline/world_generation/template_files/webots_environment/worlds/test_world.wbt\')')

    if params.submaps_scale is None:
        max_spawn_y = 1.4
    else:
        max_spawn_y = params.submaps_scale + 0.4
    print (f'Spawn y value (UP) set to {max_spawn_y}m')

    manager = dgp.SimulationManager(robot_name='krock2', ros_supervisor_name='ros_supervisor', 
                                    patch_simulation_duration=SIMULATION_TIME_PER_SUBMAP, 
                                    runs_per_submap=SIMULATION_RUNS_PER_SUBMAP,
                                    target_offset=SIMULATION_DISTANCE_OFFSET,
                                    fixed_poses=fixed_poses,
                                    max_spawn_y=max_spawn_y,
                                    )
    manager.run_simulations(world_folder, bags_folder)

    print('Done running simulations.')


# Post processing -------------------------------------------------------------------------------------------
#if run_post_processing:
#    print('Post-processing trajectories...')
#    analyzer = dgp.TrajectoryAnalyzer()
#    analyzer.extract_results(bags_folder, results_folder)
#    print('Done post-processing trajectories...')

