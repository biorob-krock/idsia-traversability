# Reza Summar blog

## 02.09.2020
* Re-calibrated `OptiTrack` cameras
* Recorded three instances of a 6cm obstacle in `Motion Capture` room
* Replicated the same experiment in `Webots`

## 30.08.2020
* Started the simulation with the all the new and old (Thomas Havy code) terrains
* Successfully calibrated OptiTrack motion capture system
* Note: the lights in Motion Capture area should be off

## 29.08.2020
* Changed the pipeline to the latest discussed version
	* In this pipeline, the submaps are douple in size (150 by 150 pixels)
	* The robot has 6 seconds to traverse 0.5 meters in the terrain
	* Each bag file contains pose+joint topic messages
	
## 20.07.2020  

* The solution we took was that we created a secondary `solid` inside `Krock` tree. Then, we set the `density` of this solid node to a very low value that caused the floating of `Krock`
* It seems that the weight of the krock is not symmetric with respect to x axis: can be originated from the mass or the control commands (TBD)

## 16.07.2020  
* Finally managed to float the robot: the key point is that in the `immersionProperties` of the `solid` node, the `fluid` environment should be specified
* The solution we took was that we created a secondary `solid` inside `Krock` tree. Then, we set the `density` of this solid node to a very low value that caused the floating of `Krock`
* It seems that the weight of the krock is not symmetric with respect to x axis: can be originated from the mass or the control commands (TBD)

## 15.07.2020  
* `density` -1 means the `mass` field must be specified. If `density` is specified, `mass` is density.vol in which vol stands for the  volume of the geometrical primitives composing the `boundingObject`
* [Same rule applies for mass field] [denw]

## 14.07.2020  
* bouyancy of the robot
* [referenceArea](https://cyberbotics.com/doc/reference/immersionproperties) has two options, we need to use `immersed area` since the other option is only availble for simple geometries defined in Webots (we are using points, probably derived from CAD meshes)
* understanding krock2 tree
* define immersionProperties
* convert krock2 into proto node
  - Matt contacted Webots Discord and it would take some work since we are using custom-defined shaped (e.g. MX64 motor mesh). We are holding off to preserved code readability. 

## 13.07.2020 Meeting with Thomas and Francesco
* Thomas mentioned that he is not rotating the maps with respect to the head orientation as we guessed
* The empty corners are due to the rotation just to have more data. This is done before simulation and doesn't change anything in Simulation
* The position of the robot is always roughly at the center of the patch. Therefore, it doesn't cover the corners of the patch
* The robot always starts from left and advances to right
* It might be the case that the black corners can bring a bias to the learning procedure which can be addressed later on by various solutions
* Thomas is saving the trajectories and sensor information of the robot in rosbag files. The reasoning is that if later we want to use the robot camera for recording the files will be heavy and rosbag would be a viable option
* For now we can change rosbag to a simpler file format to facilitate learning

## 09.07.2020
The aim is to define krock [`ImmersionProperties`][wbip], and we are taking `Salamander PROTO` example to analyze it and have a better vision. In the file `Salamander PROTO,` there are eleven nodes for which an `ImmersionProperties` is defined. Out of these 11 nodes, there are five parts of the robot with a unique `ImmersionProperties`. This means that there are six different parts of the robot that use apriori defined `ImmersionProperties`:

1. `IMMERSION_PROP_TIBIA`: only drag force is defined, `reference_area`: `Immeresed area`, others: default mode
    - line 223:227 
        ```sh
        contactMaterial "leg"
            immersionProperties [
                DEF IMMERSION_PROP_TIBIA ImmersionProperties {
                    fluidName "fluid"
                    dragForceCoefficients 0.95 0.15 0.15
                }
            ]
        ```
    - This is front left leg without the link to the body
    - Location in webots tree: Robot/children/HingeJoint/endPoint DEF SOLID_THORAX Solid/children/DEF HINGE_JOINT_LEG_1/endPoint DEF SOLID_LEG_1 Solid/children/DEF JOINT_TIBIA_1 Solid/ImmersionProperties
2. IMMERSION_PROP_FEMUR -> only drag force
    - line 246:250
        ```sh
        name "leg 1"
            contactMaterial "body"
                immersionProperties [
                  DEF IMMERSION_PROP_FEMUR ImmersionProperties {
                    fluidName "fluid"
                    dragForceCoefficients 0 0.15 0.15
                  }
                ]
        ```
    - This is the front left leg with the link to the body
    - location in webots tree: Robot/children/HingeJoint/endPoint DEF SOLID_THORAX/Solid/children/DEF HINGE_JOINT_LEG_1/endPoint DEF SOLID_LEG_1 Solid/immersionProperties

    **Note:** The first two properties are exactly used three more times. This means they are used for the other three legs of `Salamander` which indicates that we have found 8 out of 11 nodes with `ImmersionProperties`.
3. DEF IMMERSION_PROP_SEGMENT: 
	- line 714:718
		```sh
		contactMaterial "body"
                                                    immersionProperties [
                                                      DEF IMMERSION_PROP_SEGMENT ImmersionProperties {
                                                        fluidName "fluid"
                                                        referenceArea "xyz-projection"
                                                        dragForceCoefficients 3.1 1.3 0
                                                        viscousResistanceTorqueCoefficient 5
                                                      }
                                                    ]
		```
	- This is the last segment of the robot
	  [![goh](https://gitlab.com/biorob-krock/idsia-traversability/-/raw/epfl-develop/doc/final_segment.png)]()
	- Used exactly 6 times that corresponds to number of similar segments
4. DEF IMMERSION_PROP_BIG_SEGMENT -> drag force + `viscousResistanceTorqueCoefficient` + `referenceArea` = `xyz-projection`
	- line 755:760
		```sh
		contactMaterial "body"
								  immersionProperties [
									DEF IMMERSION_PROP_BIG_SEGMENT ImmersionProperties {
									  fluidName "fluid"
									  referenceArea "xyz-projection"
									  dragForceCoefficients 1.8 0.95 0.75
									  viscousResistanceTorqueCoefficient 5
									}
								  ]
		```
	- location in tree: Robot/children/HingeJoint/endPoint DEF SOLID_THORAX Solid/children/HingeJoint/endPoint DEF SEGMENT_1 Solid/children/HingeJoint/endPoint DEF SEGMENT_3 Solid/children/HingeJoint/endPoint DEF SEGMENT_PELVIS Solid/immersionProperties
	- This is the half rear of the robot
	  [![asd](https://gitlab.com/biorob-krock/idsia-traversability/-/raw/epfl-develop/doc/rear_robot.png)]()
	- Used in another node. This node highlights the entire robot except from the head (double of the half segment)
	  [![gf](https://gitlab.com/biorob-krock/idsia-traversability/-/raw/epfl-develop/doc/entire_rob.png)]()
5. DEF IMMERSION_PROP_HEAD: -> drag force + `viscousResistanceTorqueCoefficient` + `referenceArea` = `xyz-projection`
	- line 816:820
	```sh
	contactMaterial "body"
    immersionProperties [
      DEF IMMERSION_PROP_HEAD ImmersionProperties {
        fluidName "fluid"
        referenceArea "xyz-projection"
        dragForceCoefficients 2 1 1
        viscousResistanceTorqueCoefficient 5
      }
    ]
	```
	- loc in tree: robot/immersionProperties
	- Only the head

## 22.06.2020 Meeting with Thomas Havy
- chatted with Thomas Havy on data generation

## 29.06.2020 Milestone 1
- The new random way of patch generation is implemented
- To generate a world, the [`simplexNoise`][smpn] algorithm is used
- Then, the modifications to the files [`run_new_pipeline.py`][pipadd] and [`patch_generation.py`][ptadd] make sure to truncate patches from this world map
- Utilizing this method, it's easy to generate thousands of patches with no issues

## 08.07.2020
- meeting with Omar
- Unclear yet if the rotation of the patches are correct or even there
- Francesco will provide a script so we can view the hdf5 files
- Searching through the Salamander PROTO to find how the Imeersion properties are defined

## 06.07.2020
- Amphibious regions can be generated with no issues

## 02.07.2020 Lab Return
- Talked about the generation of new patches that contain fluid nodes
- First step would be to immerse Krock in a pool
- Should define drag forces with a model with four lumps
- Bouyancy properties should be dfined as well similar to salamander

[//]: #
[smpn]: <https://www.youtube.com/watch?v=Lv9gyZZJPE0>
[pipadd]: <https://gitlab.com/biorob-krock/idsia-traversability/-/blob/epfl-develop/core/run_new_pipeline.py>
[ptadd]: <https://gitlab.com/biorob-krock/idsia-traversability/-/blob/epfl-develop/core/data_generation_pipeline/patch_generation/patch_generation.py>
[wbip]: <https://cyberbotics.com/doc/reference/immersionproperties>
[denw]: <https://cyberbotics.com/doc/reference/physics>



